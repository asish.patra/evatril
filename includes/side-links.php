<div class="list-group list-group-flush">
    <h3 class="list-group-heading">Evatril Services</h3>
    <a href="services.php" class="list-group-item active list-group-item-action"><i class="icon-catering"></i> Catering Services <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-christmas"></i> Decoration <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-film"></i> Photo/Videography <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-car"></i> Vehicle Booking <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-wedding-arch"></i> Beautician <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-pastor"></i> Priest & Pandit <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-dj-o1"></i> DJ & Music <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-event1"></i> Event Planner <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-cinema"></i> Market Place <span class="icon-keyboard_arrow_right"></span></a>
    <a href="evatril-services.php" class="list-group-item list-group-item-action"><i class="icon-praying"></i> Puja Samagri <span class="icon-keyboard_arrow_right"></span></a>
</div>