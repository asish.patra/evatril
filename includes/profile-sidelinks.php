<div class="side-menu mt-3 pr-0 w-auto">
    <div class="card">
        <div class="py-3">
            <div class="list-group list-group-flush">
            <a href="userProfile.php" class="list-group-item list-group-item-action"><i class="icon-user-circle"></i> Account Setting <span class="icon-keyboard_arrow_right"></span></a>
            <a href="allOrderList.php" class="list-group-item list-group-item-action"><i class="icon-shopping1"></i> My Bookings <span class="icon-keyboard_arrow_right"></span></a>
            <a href="upcoming-booking.php" class="list-group-item list-group-item-action"><i class="icon-catering"></i> My Upcoming Booking <span class="icon-keyboard_arrow_right"></span></a>
            <a href="#" class="list-group-item list-group-item-action"><i class="icon-catering"></i> Cancel My Booking <span class="icon-keyboard_arrow_right"></span></a>
            <a href="#" class="list-group-item list-group-item-action"><i class="icon-catering"></i> My Booking History <span class="icon-keyboard_arrow_right"></span></a>
            <a href="review-ratings.php" class="list-group-item list-group-item-action"><i class="icon-catering"></i> My Reviews & Rating <span class="icon-keyboard_arrow_right"></span></a>
            <a href="#" class="list-group-item list-group-item-action"><i class="icon-mail-envelope-closed"></i> My Chats <span class="icon-keyboard_arrow_right"></span></a>
            <a href="index.php" class="list-group-item list-group-item-action"><i class="icon-power_settings_new"></i> Logout <span class="icon-keyboard_arrow_right"></span></a>
            </div>
        </div>                
    </div>
</div>