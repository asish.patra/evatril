<header class="header">
    <div class="container">
        <div class="row">
                <div class="col-lg-8 col">
                    <nav class="navbar navbar-expand-lg align-items-center px-0">
                        <a class="navbar-brand" href="index.php"><img src="imgs/evatril-logo.png" alt=""></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="icon-menu"></i>
                        </button>
                    
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="top-menu ml-lg-3">
                                <li><a href="index.php" class="nav-link active"><i class="icon-home"></i> <span class="d-lg-none">Home</span></a></li>
                                
                                <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                <i class="icon-gps"></i> Bhubaneswar
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Cuttack</a>
                                    <a class="dropdown-item" href="#">Balasore</a>
                                    <a class="dropdown-item" href="#">Bhadrak</a>
                                    <a class="dropdown-item" href="#">Rourkela</a>
                                    <a class="dropdown-item" href="#">Berhampur</a>
                                    <a class="dropdown-item" href="#">Nayagarh</a>
                                    <a class="dropdown-item" href="#">Jagatsinghpur</a>
                                    <a class="dropdown-item" href="#">Khurda</a>
                                    <a class="dropdown-item" href="#">Jharsuguda</a>
                                    <a class="dropdown-item" href="#">Kalahandi</a>
                                </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"> Services
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Venue Selection</a>
                                        <a class="dropdown-item" href="#">Vehicle Booking</a>
                                        <a class="dropdown-item" href="#">Choreography</a>
                                        <a class="dropdown-item" href="#">Menu Selection</a>
                                        <a class="dropdown-item" href="#">DJ</a>
                                        <a class="dropdown-item" href="#">Beautician</a>
                                        <a class="dropdown-item" href="#">Decoration</a>
                                        <a class="dropdown-item" href="#">Photo/Videography</a>
                                        <a class="dropdown-item" href="#">Pandir/Priest</a>
            
                                    </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"> Products
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Product 1</a>
                                            <a class="dropdown-item" href="#">Product 2</a>
                                            <a class="dropdown-item" href="#">Product 3</a>
                                        </div>
                                        </li>
                                <li><a href="#" class="nav-link"> Blog</a></li>
                            </ul>
                            
                            
                        </div>
                        
                    </nav>

                </div>
                <div class="col-lg-4 col my-auto">
                    <div class="userControl">
                        <div class="mob-no ml-auto">
                            <a href="tel:+8917220478"><i class="icon-phone-call"></i></a>
                        </div>
                        <div class="action-btns d-none">
                            <a href="#" data-toggle="modal" data-target="#signupModal">Sign Up</a>
                             <a href="#" data-toggle="modal" data-target="#loginModal">Login</a>
                        </div>
                        <div class="shpCart ml-sm-0 ml-auto">
                            <a href=""><i class="icon-heart"></i><small>10</small></a>
                        </div>
                        <div class="userSetting dropdown">
                            <a href="" class="profilePic dropdown-toggle" data-display="static" data-toggle="dropdown"><img src="imgs/user.jpg" alt=""></a>
                            <div class="dropdown-menu">
                            <p class="userName"><small>Welcome</small>Mr. Jayant Tripathy </p>
                            <a class="dropdown-item" href="userProfile.php"><i class="icon-user-circle-o"></i> Profile</a>
                            <a class="dropdown-item" href="allOrderList.php"><i class="icon-cart1"></i> My Orders</a>
                            <a class="dropdown-item" href="index.php"><i class="icon-exit_to_app"></i> Sign Out</a>
                        </div>
                        </div>
                    </div>
                </div>

        </div>
        
        
    </div>
</header>