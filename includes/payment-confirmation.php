<div class="row align-items-center">
    <div class="col-sm-5">Expected Guest No.</div> 
    <div class="col-sm-7"><input type="text" class="form-control" placeholder="No. of plates"></div>
</div>
<div class="estimated-cost-wrapper">
    <div class="amout">
        <p>Estimated: <strong><i class="icon-inr"></i> 1,05,000</strong></p>
        <p>Plate Price: <strong><i class="icon-inr"></i> 450</strong></p>
        <p class="mb-0">No. of Plate: <strong>300</strong></p>
    </div>
    <div class="advance">
        <p>20% of the Estimated Cost in Advance</p>
        <p class="mb-0"><small>Booking Amount:</small> <br><strong><i class="icon-inr"></i> 21,000</strong></p>
    </div>
</div>

<div class="text-right">
    <button class="btn btn-sm btn-primary px-4" onclick="window.location.href = 'service-confirm-booking.php';">Continue <i class="icon-arrow-right"></i></button>
</div>