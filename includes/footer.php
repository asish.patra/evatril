<footer class="pt-5 pb-4">
    <div class="container pt-3">
        <div class="row">
            <div class="col-sm-3 mb-sm-0 mb-5">
                <img src="imgs/evatril-logo.png" alt="">
                <p class="my-sm-5 my-4">We want to help you plan an event that rocks! An event so superbly run that guests rave about it to everyone they know</p>
                <ul class="social-links">
                    <li><a href="#" class="fb"><i class="icon-social-facebook"></i></a></li>
                    <li><a href="#" class="twitter"><i class="icon-social-twitter"></i></a></li>
                    <li><a href="#" class="ln"><i class="icon-social-linkedin"></i></a></li>
                    <li><a href="#" class="insta"><i class="icon-instagram"></i></a></li>
                </ul>
                <div class="row mt-4">
                    <div class="col-6"><a href="#"><img src="imgs/play-store.png" alt=""></a></div>
                    <div class="col-6"><a href="#"><img src="imgs/app-store.png" alt=""></a></div>
                </div>
            </div>
            <div class="col-sm-3 mb-sm-0 mb-4">
                <h2>Useful Links</h2>
                <ul class="footer-links">
                    <li><a href="#">Venue Selection</a></li>
                    <li><a href="#">Menu Selection</a></li>
                    <li><a href="#">Decoration</a></li>
                    <li><a href="#">Photo/Videography</a></li>
                    <li><a href="#">Choreography</a></li>
                    <li><a href="#">Beautician</a></li>
                    <li><a href="#">Pandit/Priest</a></li>
                    <li><a href="#">DJ/Music</a></li>
                    <li><a href="#">Event Planner</a></li>
                </ul>
            </div>
            <div class="col-sm-3 mb-sm-0 mb-4">
                <h2>Jewellery</h2>
                <ul class="footer-links">
                    <li><a href="#">Ring</a></li>
                    <li><a href="#">Earning</a></li>
                    <li><a href="#">Pendants</a></li>
                    <li><a href="#">Necklace</a></li>
                    <li><a href="#">Bangles</a></li>
                </ul>
                <div class="d-flex mt-4 align-items-center">
                    <a href="#" class="mr-3"><img src="imgs/marchant-logo.jpg" width="56px" alt="marchant mobile app" title="Marchant APP"></a>
                    <a href="#" class="btn btn-primary px-4 text-uppercase">Sign Up / Login</a>
                </div>
            </div>
            <div class="col-sm-3">
                <h2>Quick Links</h2>
                <ul class="footer-links">
                    <li><a href="#">Terms</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Blogs</a></li>
                    <li><a href="#">Testimonials</a></li>
                    <li><a href="#">Support</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Merchant Signup</a></li>
                </ul>
            </div>
        </div>
        <p class="copyright-text text-center mt-sm-5 mt-2 mb-0 pt-4">Copyright © 2019, All Rights Reserved | Evatril</p>
    </div>
</footer>

<!-- Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-body p-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row m-0">
                <div class="col-sm-6 login-img text-center p-3">
                    <img src="imgs/evatril-logo.png" alt="">

                </div>
                <div class="col-sm-6 p-5">
                    <h3 class="mb-4">Log In</h3>
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter here">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Enter here">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block text-uppercase mt-4">Login</button>
                        <p class="mt-4 text-center text-muted">New User? <a href="javascript: void(0);" id="open_signup">SIGN UP</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<!-- Signup Modal -->
<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-body p-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="row m-0">
                <div class="col-sm-6 login-img text-center p-3">
                    <img src="imgs/evatril-logo.png" alt="">

                </div>
                <div class="col-sm-6 p-5">
                    <h3 class="mb-4">Sign Up</h3>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" aria-describedby="name" placeholder="Full Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="mobile" aria-describedby="mobile" placeholder="Mobile Number">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" aria-describedby="email" placeholder="Email ID">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="repeat-password" placeholder="Repeat Password">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block text-uppercase mt-4">Signup</button>
                        <p class="mt-4 text-center text-muted">New User? <a href="javascript: void(0);" id="open_login">SIGN IN</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function() {
        /*Fixed header Start*/
				$(window).scroll(function () {
					var scroll = $(window).scrollTop();

					if (scroll >= 200) {
						$(".header").addClass("header-fixed");
					} else {
						$(".header").removeClass("header-fixed");
					}
				});
		/*Fixed header End*/
        
    });
    $(function(){
        $('#open_login').click(function(){
            $('#signupModal').modal('hide')
            $('#loginModal').modal('show')
        });
        $('#open_signup').click(function(){
            $('#loginModal').modal('hide')
            $('#signupModal').modal('show')
        });
    })
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format: "dd-M-yyyy",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        });
    });
    $(document).ready(function(){
        // $('.side-menu .list-group-item').each(function() {
        //     var back = ["#669be8","#ff0000","#ff6766","#ffcc4c","#ff9800","#9bf138","#a374f7","#669be8","#ff0000","#ccc","#ffcccc","#ff9800","#9bf138","#a374f7"];
        //     var rand = back[Math.floor(Math.random() * back.length)];
        //     $(this).find("i").css('color',rand);
        // });
    });
    // $(document).on('click', 'a[href^="#"]', function (event) {
    //     event.preventDefault();

    //     $('html, body').animate({
    //         scrollTop: $($.attr(this, 'href')).offset().top - 150
    //     }, 500);
    // });
</script>