
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 inner-wrapper py-3">
                <div class="side-menu">
                    <div class="card pb-2">
                        <?php include 'includes/side-links.php'; ?>
                    </div>
                </div>
                <div class="inner-content p-4">
                    <div class="filter p-3 mb-3">
                        <div class="row">
                            <div class="col-lg-2 pr-lg-0 mb-lg-0 mb-2 filterInput">
                                <select class="form-control">
                                    <option value="0">Occasion</option>
                                </select>
                            </div>
                            <div class="col-lg-2 pr-lg-0 mb-lg-0 mb-2 filterInput">
                                <select class="form-control">
                                    <option value="0">Location</option>
                                    <option value="patia">Patia</option>
                                </select>
                            </div>
                            <div class="col-lg-2 pr-lg-0 mb-lg-0 mb-2 filterInput">
                                <div class="input-group date-pick">
                                    <input type="text" class="form-control datepicker" id="date" placeholder="dd/mm/yyyy">
                                    <div class="input-group-append">
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 pr-lg-0 mb-lg-0 mb-2 filterInput">
                                <select class="form-control">
                                    <option value="0">Venue Type</option>
                                    <option value="mandap">Mandap / Standalone</option>
                                    <option value="hotel">Hotel</option>
                                    <option value="starHotel">5 Star Hotel</option>
                                    <option value="resort">Resort</option>
                                    <option value="premium">Premium</option>
                                    <option value="community">Community Hall</option>
                                    <option value="farmHouse">Farm House</option>
                                    <option value="special">Special</option>
                                </select>
                            </div>
                            <div class="col-lg-2 pr-lg-0 mb-lg-0 mb-2">
                                <div class="button-group">
                                    <button type="button" class="btn btn-primary btn-block dropdown-toggle" data-toggle="dropdown">Filter</button>
                                    <div class="dropdown-menu filter-dropdown">
                                        <a href="#" class="list-group-item list-group-item-action" data-value="option1" tabIndex="-1"><input type="checkbox" value="rental"/>&nbsp;Rental Booking</a>
                                        <a href="#" class="list-group-item list-group-item-action" data-value="option2" tabIndex="-1"><input type="checkbox" value="plate"/>&nbsp;Plate Booking</a>
                                        <a href="#" class="list-group-item list-group-item-action" data-value="option3" tabIndex="-1"><input type="checkbox" value="ac"/>&nbsp;A/C</a>
                                        <a href="#" class="list-group-item list-group-item-action" data-value="option4" tabIndex="-1"><input type="checkbox" value="nac"/>&nbsp;Non A/C</a>
                                        <a href="#" class="list-group-item list-group-item-action" data-value="option5" tabIndex="-1"><input type="checkbox" value="nac"/>&nbsp;Veg</a>
                                        <a href="#" class="list-group-item list-group-item-action" data-value="option6" tabIndex="-1"><input type="checkbox" value="nac"/>&nbsp;Non Veg</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                
                                <div class="btn-group d-flex" role="group" aria-label="Basic example">
                                    <button type="button" class="btn search-btn btn-outline-primary rounded-left mr-1 d-lg-none"><span class="icon-search"></span></button>
                                    <button class="btn btn-primary btn-block">SEARCH</button>
                                  </div>
                            </div>
                        </div>
                    </div>

                    <div class="row venue-detail-info">
                        <div class="col-lg-3  lft-part">
                            <a href="venue-details.php"><img src="imgs/venue-img1.jpg" alt=""></a>
                            <p class="rattings-btn mb-3 mb-sm-0 mt-3">
                                <strong><i class="icon-star mr-1"></i> 2.5</strong>
                                <a href="#">4 Reviews</a>
                            </p>
                        </div>
                        <div class="col-lg-9 rgt-part">
                            <a href="venue-details.php"><h2>Monty kanna road, <small>Patia, Bhubaneswar</small></h2></a>
                            <div class="row mb-0 mt-5 mb-sm-5">
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-air-conditioner text-success"></i>
                                        <strong>A/C  </strong> <label><i class="icon-check"></i> Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-cinema text-info"></i>
                                        <strong>Venue Type</strong> <label>Hotel</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-lunch1 text-warning"></i>
                                    <strong>Per Plate  </strong><label><i class="icon-inr"></i> 2500 - <i class="icon-inr"></i> 3500</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-wedding-cake text-secondary"></i>
                                    <strong>Food Allowed  </strong><label>Non-Veg</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-outline-primary mr-sm-3"><i class="icon-phone-call"></i> 2344940932</button>
                                    <button class="btn btn-primary mt-3 mt-sm-0" onclick="window.location.href = 'venue-details.php';">Find Availability</button>
                                </div>
                            </div>

                        </div>
                    </div>




                    <div class="row venue-detail-info">
                        <div class="col-lg-3  lft-part">
                            
                            <a href="venue-details.php"><img src="imgs/venue-img2.jpg" alt=""></a>
                            <p class="rattings-btn mb-3 mb-sm-0 mt-3">
                                <strong><i class="icon-star mr-1"></i> 2.5</strong>
                                <a href="#">4 Reviews</a>
                            </p>
                        </div>
                        <div class="col-lg-9 rgt-part">
                            
                            <a href="venue-details.php"><h2>Mayfair Lagoon, <small>Niladri Vihar, Bhubaneswar</small></h2></a>
                            <div class="row mb-0 mt-5 mb-sm-5">
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-air-conditioner text-success"></i>
                                        <strong>A/C  </strong> <label><i class="icon-check"></i> Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-cinema text-info"></i>
                                        <strong>Venue Type</strong> <label>Hotel</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-lunch1 text-warning"></i>
                                    <strong>Per Plate  </strong><label><i class="icon-inr"></i> 2500 - <i class="icon-inr"></i> 3500</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-wedding-cake text-secondary"></i>
                                    <strong>Food Allowed  </strong><label>Non-Veg</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-outline-primary mr-sm-3"><i class="icon-phone-call"></i> 2344940932</button>
                                    <button class="btn btn-primary mt-3 mt-sm-0" onclick="window.location.href = 'venue-details.php';">Find Availability</button>
                                </div>
                            </div>

                        </div>
                    </div>



                    <div class="row venue-detail-info">
                        <div class="col-lg-3  lft-part">
                            <a href="venue-details.php"><img src="imgs/venue-img3.jpg" alt=""></a>
                            <p class="rattings-btn mb-3 mb-sm-0 mt-3">
                                <strong><i class="icon-star mr-1"></i> 2.5</strong>
                                <a href="#">4 Reviews</a>
                            </p>
                        </div>
                        <div class="col-lg-9 rgt-part">
                            
                            <a href="venue-details.php"><h2>Swosti Premium, <small>Jayadev Vihar, Bhubaneswar</small></h2></a>
                            
                            <div class="row mb-0 mt-5 mb-sm-5">
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-air-conditioner text-success"></i>
                                        <strong>A/C  </strong> <label><i class="icon-check"></i> Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-cinema text-info"></i>
                                        <strong>Venue Type</strong> <label>Hotel</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-lunch1 text-warning"></i>
                                    <strong>Per Plate  </strong><label><i class="icon-inr"></i> 2500 - <i class="icon-inr"></i> 3500</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-wedding-cake text-secondary"></i>
                                    <strong>Food Allowed  </strong><label>Non-Veg</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-outline-primary mr-sm-3"><i class="icon-phone-call"></i> 2344940932</button>
                                    <button class="btn btn-primary mt-3 mt-sm-0" onclick="window.location.href = 'venue-details.php';">Find Availability</button>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row venue-detail-info">
                        <div class="col-lg-3  lft-part">
                            <a href="venue-details.php"><img src="imgs/venue-img4.jpg" alt=""></a>
                            <p class="rattings-btn mb-3 mb-sm-0 mt-3">
                                <strong><i class="icon-star mr-1"></i> 2.5</strong>
                                <a href="#">4 Reviews</a>
                            </p>
                        </div>
                        <div class="col-lg-9 rgt-part">
                            
                            <a href="venue-details.php"><h2>Hotel Ginger, <small>Jayadev Vihar, Bhubaneswar</small></h2></a>
                            
                            <div class="row mb-0 mt-5 mb-sm-5">
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-air-conditioner text-success"></i>
                                        <strong>A/C  </strong> <label><i class="icon-check"></i> Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-cinema text-info"></i>
                                        <strong>Venue Type</strong> <label>Hotel</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-lunch1 text-warning"></i>
                                    <strong>Per Plate  </strong><label><i class="icon-inr"></i> 2500 - <i class="icon-inr"></i> 3500</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-wedding-cake text-secondary"></i>
                                    <strong>Food Allowed  </strong><label>Non-Veg</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-outline-primary mr-sm-3"><i class="icon-phone-call"></i> 2344940932</button>
                                    <button class="btn btn-primary mt-3 mt-sm-0" onclick="window.location.href = 'venue-details.php';">Find Availability</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
    
    <?php include 'includes/footer.php'; ?>
    <script>
        $(document).ready(function(){
            if($(window).width() <= 991){
                $(".filterInput").hide();
                $(".filter .search-btn").click(function(){
                    $(".filterInput").slideToggle();
                });
            }


            // multi select checkbox dropdown list
            var options = [];

            $( '.filter-dropdown a' ).on( 'click', function( event ) {

            var $target = $( event.currentTarget ),
                val = $target.attr( 'data-value' ),
                $inp = $target.find( 'input' ),
                idx;

            if ( ( idx = options.indexOf( val ) ) > -1 ) {
                options.splice( idx, 1 );
                setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } else {
                options.push( val );
                setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }

            $( event.target ).blur();
                
            console.log( options );
            return false;
            });
        });
    </script>
</body>
</html>