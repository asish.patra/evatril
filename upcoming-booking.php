
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-4 pr-sm-0">
            <div class="whiteBox p-3 d-flex">
                <img src="imgs/profile.png" class="mr-3" alt="" width="45" height="45">
                <h4><small class="d-block">Hello,</small>Himalay Pagada</h4>
            </div>
            <?php include 'includes/profile-sidelinks.php'; ?>

          </div>
          <div class="col-lg-9 col-sm-8">
            <h3 class="mb-sm-3 mb-4">My Upcoming Bookings</h3>
            <div class="card mb-3">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-sm-6">
                    <label class="h5"><small class="text-muted">Booking No.</small><strong> #4224234</strong></label> <a href="#" class="btn ml-2"><i class="icon-file-pdf-o"></i> Invoice</a>
                  </div>
                  <div class="col-sm-6 text-sm-right">
                    <label class="mr-3"> <i class="icon-calendar"></i> 22 Mar 2020 </label>
                    <button class="btn btn-danger px-3 text-uppercase btn-sm">Cancel Booking</button>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <img src="imgs/food-img3.jpg" class="w-100" alt="">
                  </div>
                  <div class="col-sm-4 pr-0">
                    <h5 class="font-weight-bold">Diamond Veg</h5>
                    <p class="m-0 mt-3">Plate : <i class="icon-inr"></i> 450/-</p>
                    <span>Booking Payment : <i class="icon-inr"></i> 21,000/-</span>
                  </div>
                  <div class="col-sm-5">
                    <ul class="booked-info-list">
                      <li><span class="icon-press"></span> Occasion : <strong>Weeding Ceremony</strong></li>
                      <li><span class="icon-press"></span> Add On : <strong class="text-success"><i class="icon-check h4 align-middle"></i> Yes</strong></li>
                      <li><span class="icon-press"></span> Number of Items : <strong>37</strong></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="card mb-3">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-sm-6">
                    <label class="h5"><small class="text-muted">Booking No.</small><strong> #4224234</strong></label> <a href="#" class="btn ml-2"><i class="icon-file-pdf-o"></i> Invoice</a>
                  </div>
                  <div class="col-sm-6 text-sm-right">
                    <label class="mr-3"> <i class="icon-calendar"></i> 22 Mar 2020 </label>
                    <button class="btn btn-danger px-3 text-uppercase btn-sm">Cancel Booking</button>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <img src="imgs/food-img3.jpg" class="w-100" alt="">
                  </div>
                  <div class="col-sm-4 pr-0">
                    <h5 class="font-weight-bold">Diamond Veg</h5>
                    <p class="m-0 mt-3">Plate : <i class="icon-inr"></i> 450/-</p>
                    <span>Booking Payment : <i class="icon-inr"></i> 21,000/-</span>
                  </div>
                  <div class="col-sm-5">
                    <ul class="booked-info-list">
                      <li><span class="icon-press"></span> Occasion : <strong>Weeding Ceremony</strong></li>
                      <li><span class="icon-press"></span> Add On : <strong class="text-success"><i class="icon-check h4 align-middle"></i> Yes</strong></li>
                      <li><span class="icon-press"></span> Number of Items : <strong>37</strong></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="card mb-3">
              <div class="card-body">
                <h5>Reviews & Rating Policy</h5>
                <ul class="cancellation-list">
                  <li><i class="icon-food"></i> Please click on Review & Rating botton to write & rate your experiences.</li>
                  <li><i class="icon-food"></i> Your review & Rating will help to improve the service quality.</li>
                  <li><i class="icon-food"></i> Every Review & Rating will make you eligible get reward points.</li>
                </ul>
              </div>
            </div>

          </div>
        </div>
      </div>

    </section>

    <?php include 'includes/footer.php'; ?>
   
</body>
</html>