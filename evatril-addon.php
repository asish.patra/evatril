
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
    <div class="container">
        <div class="inner-wrapper">
            <div class="inner-content p-sm-4 p-3">
                <div class="row">
                    <div class="col-sm-6 mb-sm-0 mb-3">
                        <div class="package-info add-on-info pb-0 pt-2">
                            <div class="row-ln">
                                <i class="icon-catering"></i>
                                <label for="">Primary Package</label>
                                <strong class="text-secondary">Standard Decor</strong>
                                <strong><span class="icon-inr"></span> 6,000</strong>
                            </div>
                        </div>
                        <h5 class="mt-4 addon-hdng">ADD ON</h5>
                        <table class="table border add-on-menu-table">
                            <tr>
                                <td width="44px">
                                    <div class="custom-checkbox">
                                        <input type="checkbox" id="cLight" name="cLight">
                                            <label for="cLight"></label>
                                    </div>
                                </td>
                                <td>Customized Lighting</td>
                                <td width="20%" class="text-right"><strong><i class="icon-inr"></i> 2,999 /-</strong></td>
                            </tr>
                            <tr>
                                <td width="44px">
                                    <div class="custom-checkbox">
                                        <input type="checkbox" id="basicFlower" name="basicFlower">
                                            <label for="basicFlower"></label>
                                    </div>
                                </td>
                                <td>Basic Flower</td>
                                <td width="20%" class="text-right"><strong><i class="icon-inr"></i> 7,999 /-</strong></td>
                            </tr>
                            <tr>
                                <td width="44px">
                                    <div class="custom-checkbox">
                                        <input type="checkbox" id="customFlower" name="customFlower">
                                            <label for="customFlower"></label>
                                    </div>
                                </td>
                                <td>Customized Flowering</td>
                                <td width="20%" class="text-right"><strong><i class="icon-inr"></i> 4,000 /-</strong></td>
                            </tr>
                            <tr>
                                <td width="44px">
                                    <div class="custom-checkbox">
                                        <input type="checkbox" id="basicLighting" name="basicLighting">
                                            <label for="basicLighting"></label>
                                    </div>
                                </td>
                                <td>Basic Lighting</td>
                                <td width="20%" class="text-right"><strong><i class="icon-inr"></i> 2,999 /-</strong></td>
                            </tr>
                        </table>
                        <h4 class="mt-4">How it Works</h4>
                        <div class="how-it-works text-center">
                            <img src="imgs/how-it-works.png" alt="">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            <div class="py-3"></div>
                            <img src="imgs/how-it-works1.png" alt="">
                            <p>Hic quae dolor! Mollitia sed neque labore recusandae necessitatibus pariatur.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 pl-sm-0">
                        <div class="row pr-3">
                            <div class="col-sm-4 p-0 pl-3 mb-2 mb-lg-0"><img src="imgs/venue-decor.jpg" alt=""></div>
                            <div class="col-sm-4 p-0 pl-3 mb-2 mb-lg-0"><img src="imgs/venue-decor.jpg" alt=""></div>
                            <div class="col-sm-4 p-0 pl-3"><img src="imgs/venue-decor.jpg" alt=""></div>
                        </div>
                        <div class="paymentDet px-0">
                            <table class="table table-borderless border-bottom mb-3">
                                <tr>
                                    <td>
                                        <strong class="h5">Basic Lighting Decor</strong>
                                    </td>
                                    <td width="25%" class="text-right text-primary"><strong><i class="icon-inr"></i> 2,999 /-</strong></td>
                                    <td width="64px" class="text-right">
                                        <div class="custom-checkbox ml-auto">
                                            <input type="checkbox" id="basicLighting" name="basicLighting">
                                                <label for="basicLighting"></label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="pkgItemsDet">
                                <ul class="package-detail-list">
                                    <li>
                                        <i class="icon-box"></i>
                                        Birthday, Anniversary, Small Party Packages
                                    </li>
                                    <li>
                                        <i class="icon-box"></i>
                                        Birthday, Anniversary, Small Party Packages
                                    </li>
                                    <li>
                                        <i class="icon-box"></i>
                                        Birthday, Anniversary, Small Party Packages
                                    </li>
                                    <li>
                                        <i class="icon-box"></i>
                                        Birthday, Anniversary, Small Party Packages
                                    </li>
                                </ul>
                                
                            </div>                                 
                            <div class="text-right pt-3">
                                <button class="btn btn-sm btn-primary px-5" data-toggle="modal" data-target="#advanceModal">Proceed <i class="icon-arrow-right"></i></button>
                            </div>                  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    
    <?php include 'includes/footer.php'; ?>

<!-- Advance Modal -->
<div class="modal fade" id="advanceModal" tabindex="-1" role="dialog" aria-labelledby="advanceModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="advanceModalLabel">Payment Informations</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="estimated-cost-wrapper pt-0">
                <div class="amout">
                    <p>Primary Package: <strong><br><i class="icon-inr"></i> 6,000</strong></p>
                    <p>ADDON Price: <strong><i class="icon-inr"></i> 6,998</strong></p>
                    <p>Total : <strong class="text-primary"><i class="icon-inr"></i> 12,998</strong></p>
                </div>
                <div class="advance">
                    <p>20% of the Estimated Cost in Advance</p>
                    <p class="mb-0"><small>Booking Amount:</small> <br><strong><i class="icon-inr"></i> 1,800</strong></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <a href="evatril-addon.php" class="btn btn-outline-primary btn-block"><i class="icon-plus"></i> ADD ON</a>
                </div>
                <div class="col-sm-6 text-right">
                    <button class="btn btn-sm btn-primary px-4" onclick="window.location.href = 'service-confirm-booking.php';">Continue <i class="icon-arrow-right"></i></button>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

</body>
</html>