<?php include 'includes/doctype.php'; ?>

<body>

    <?php include 'includes/header.php'; ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 inner-wrapper py-3">
                <div class="container">
                    <div class="inner-content p-4">
                        <div class="row">
                            <div class="col-sm-4 mt-5 mt-sm-0 order-sidemenu">
                                <h4>FAQ (User Guide)</h4>
                                <div class="card faq-card">
                                    <h5>1. How to book a hall?</h5>
                                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nostrum quod placeat nisi voluptatibus consequuntur, culpa saepe molestiae beatae voluptas neque dignissimos tempore rem mollitia explicabo pariatur fuga nam illum iste?</p>

                                    <h5>Is their any Home delivery System ?</h5>
                                    <p>Yes, after selecting any menu, user can select home delivery option or preparation at venue</p>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <h4>Select Menu</h4>
                                <div class="row service-menu-list">
                                    <div class="col-sm-12 mb-3">
                                        <div class="card">
                                            <label class="vegType veg"></label>
                                            <a href="services-detail.php">
                                                <img src="imgs/food-img3.jpg" alt="">
                                            </a>
                                            <div class="text-content">
                                                <p>"Jain Cuisine | Sacred Food Vegetarian Cuisine"</p>
                                                <a href="services-detail.php" class="btn btn-outline-primary px-4 btn-sm">Pure Vegiterian Menu <i class="icon-keyboard_arrow_right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <div class="card">
                                            <label class="vegType veg"></label>
                                            <a href="services-detail.php">
                                                <img src="imgs/food-img3.jpg" alt="">
                                            </a>
                                            <div class="text-content">
                                                <p>"Indian Cuisine | Continental Chinese Cuisine"</p>
                                                <a href="services-detail.php" class="btn btn-outline-primary px-4 btn-sm">Non Vegiterian Menu <i class="icon-keyboard_arrow_right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <label class="vegType non-veg"></label>
                                            <a href="services-detail.php">
                                                <img src="imgs/food-img3.jpg" alt="">
                                            </a>
                                            <div class="text-content">
                                                <p>"Dry Sweet | Syrupy Sweet Special Namkeen"</p>
                                                <a href="services-detail.php" class="btn btn-outline-primary px-4 btn-sm">Welcome Sweets/Snacks <i class="icon-keyboard_arrow_right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'includes/footer.php'; ?>

</body>

</html>