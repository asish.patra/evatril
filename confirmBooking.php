
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 mt-sm-0 mt-3 pr-sm-0 order-sidemenu">
                    <div class="side-menu h-100 pr-0 w-auto">
                        <div class="card pb-2">
                            <?php include 'includes/side-links.php'; ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 mt-3 mt-sm-0">
                    <div class="thankMsg">
                        <h2 class="mb-5">Your thoughtfulness is a gift <br> we always treasure.</h2>
                        <button class="btn btn-primary">Want to book the same Venue again <span class="icon-arrow-right"></span></button>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-6 pr-sm-0 mb-3 mb-sm-0">
                            <div class="bookingDet">
                                <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Booking Details</p>
                                <span>
                                    <i class="icon-calendar"></i>
                                    <label for="">Date of Booking</label>
                                    <strong>02/04/2020</strong>
                                </span>
                                <span>
                                    <i class="icon-diamond"></i>
                                    <label for="">Occassion</label>
                                    <strong>Wedding Ceremony</strong>
                                </span>
                                <span>
                                    <i class="icon-property"></i>
                                    <label for="">Venue Booked</label>
                                    <strong>Mayfair Resort</strong>
                                </span>
                                <span>
                                    <i class="icon-catering"></i>
                                    <label for="">Slot Booked</label>
                                    <strong>Lunch <small>(01.00 P.M - 04.30 P.M)</small></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="paymentDet h-100">
                                <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Payment Details</p>
                                <span>
                                    <label for="">Final Cost</label>
                                    <strong>₹ 54,000</strong>
                                </span>
                                <span>
                                    <label for="">Booking Amount</label>
                                    <strong>₹ 10,000</strong>
                                </span>
                                <span>
                                    <label for="">Remaining Amount</label>
                                    <strong>₹ 44,000</strong>
                                </span>
                                <div class="row mt-4">
                                    <div class="col-sm-6 pr-sm-0 mb-3 mb-sm-0"><button class="btn btn-primary btn-block" data-toggle="modal" data-target="#payModal"><i class="icon-check"></i> Confirm Booking</button></div>
                                    <div class="col-sm-6"><button class="btn btn-outline-primary btn-block" data-toggle="modal" data-target="#cancelModal"><i class="icon-heart"></i> Save Later</button></div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            <div class="row mt-3">
                <div class="col-sm-4 pr-0">
                    <div class="imgAdd">
                        <img src="imgs/scene.jpg" alt="">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="terms h-100">
                        <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Terms & Conditions</p>

                        <ul>
                            <li>
                                Remaining balance will be paid on event date.
                            </li>
                            <li>
                                If any due is exceeded over estimated cost due to additional order, it is under puragative for venue policy.
                            </li>
                            <li>
                                Venue policy is very friendly towards customer so any short of out of policy can't be entertained.
                            </li>
                            <li>
                                Within 36hrs of booking, booking can be cancelled & customer can get full refund but after 48hrs from the time of booking no refund will be made.
                            </li>
                            <li>
                                Evatril reserves the right of cacellation of the venue within 36hrs from the time of booking.
                            </li>
                            <li>
                                After 48hrs time of booking venue cancellation will not be possible for both Evatril & Venue Management.
                            </li>
                            <li>
                                Evatril reserves the right of venue booking confirmation within 24hrs.
                            </li>
                            <li>All venues booded 72hrs before the event can't be cancelled. If cancelled no refund will be made.</li>
                        </ul>
                        
                    </div>
                </div>
            </div>            
        </div>
    </section>
    <!-- Modal -->
    <div id="payModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p class="display-4">Payment Gateway</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div> 
<!-- Modal -->
<div id="cancelModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 text-center">
                        <p class="display-4">Booking Cancelled</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div> 
        
    <?php include 'includes/footer.php'; ?>
   
</body>
</html>