
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-3 pr-sm-0">
            <div class="whiteBox p-3 d-flex">
                <img src="imgs/profile.png" class="mr-3" alt="" width="45" height="45">
                <h4><small class="d-block">Hello,</small>Himalay Pagada</h4>
            </div>
            <?php include 'includes/profile-sidelinks.php'; ?>

          </div>
          <div class="col-lg-9 col-sm-8">
            <h3>My Reviews & Rating</h3>
            <div class="card mb-3">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-sm-6">
                    <label class="h5"><small class="text-muted">Booking No.</small><strong> #4224234</strong></label> <a href="#" class="btn ml-2"><i class="icon-file-pdf-o"></i> Invoice</a>
                  </div>
                  <div class="col-sm-6 text-sm-right">
                    <label class="mr-3"> <i class="icon-calendar"></i> 22 Mar 2020 </label>
                    <button class="btn btn-primary px-3 text-uppercase btn-sm">Reviews & Rating</button>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <img src="imgs/venue-img1.jpg" alt="">
                  </div>
                  <div class="col-sm-4 pr-0">
                    <h5 class="font-weight-bold">Hotel Mayfair Resort</h5>
                    <p class="text-muted"><small>Ajad Road, Jayadev Vihar, Bhubaneswar-08</small></p>
                    <p class="m-0 mt-3">Package Name : <strong>Golden-Veg</strong></p>
                    <span>Advance Payment : 20% - <i class="icon-inr"></i> 10,000/-</span>
                  </div>
                  <div class="col-sm-5">
                    <ul class="booked-info-list">
                      <!-- <li><span class="icon-press"></span> Date Booked : <strong>02/02/2020 <i class="icon-calendar"></i></strong></li> -->
                      <li><span class="icon-press"></span> Occasion : <strong>Weeding Ceremony</strong></li>
                      <li><span class="icon-press"></span> Slot Booked : <strong>1:00 PM to 04:30PM</strong></li>
                      <li><span class="icon-press"></span> Estimated Cost : <strong><i class="icon-inr"></i> 54,000</strong></li>
                      <li><span class="icon-press"></span> Balance Amount : <strong><i class="icon-inr"></i> 44,000</strong></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="card mb-3">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-sm-6">
                    <label class="h5"><small class="text-muted">Booking No.</small><strong> #4224234</strong></label> <a href="#" class="btn ml-2"><i class="icon-file-pdf-o"></i> Invoice</a>
                  </div>
                  <div class="col-sm-6 text-sm-right">
                    <label class="mr-3"> <i class="icon-calendar"></i> 22 Mar 2020 </label>
                    <button class="btn btn-primary px-3 text-uppercase btn-sm">Reviews & Rating</button>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <img src="imgs/venue-img1.jpg" alt="">
                  </div>
                  <div class="col-sm-4 pr-0">
                    <h5 class="font-weight-bold">Hotel Mayfair Resort</h5>
                    <p class="text-muted"><small>Ajad Road, Jayadev Vihar, Bhubaneswar-08</small></p>
                    <p class="m-0 mt-3">Package Name : <strong>Golden-Veg</strong></p>
                    <span>Advance Payment : 20% - <i class="icon-inr"></i> 10,000/-</span>
                  </div>
                  <div class="col-sm-5">
                    <ul class="booked-info-list">
                      <!-- <li><span class="icon-press"></span> Date Booked : <strong>02/02/2020 <i class="icon-calendar"></i></strong></li> -->
                      <li><span class="icon-press"></span> Occasion : <strong>Weeding Ceremony</strong></li>
                      <li><span class="icon-press"></span> Slot Booked : <strong>1:00 PM to 04:30PM</strong></li>
                      <li><span class="icon-press"></span> Estimated Cost : <strong><i class="icon-inr"></i> 54,000</strong></li>
                      <li><span class="icon-press"></span> Balance Amount : <strong><i class="icon-inr"></i> 44,000</strong></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="card mb-3">
              <div class="card-body">
                <h5>Reviews & Rating Policy</h5>
                <ul class="cancellation-list">
                  <li><i class="icon-food"></i> Please click on Review & Rating botton to write & rate your experiences.</li>
                  <li><i class="icon-food"></i> Your review & Rating will help to improve the service quality.</li>
                  <li><i class="icon-food"></i> Every Review & Rating will make you eligible get reward points.</li>
                </ul>
              </div>
            </div>

          </div>
        </div>
      </div>

    </section>

    <?php include 'includes/footer.php'; ?>
   
</body>
</html>