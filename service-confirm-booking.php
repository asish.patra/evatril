
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 pr-sm-0 mt-sm-0 mt-3 order-sidemenu">
                    <div class="side-menu h-100 pr-0 w-auto">
                        <div class="card pb-2">
                            <?php include 'includes/side-links.php'; ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-4 pr-sm-0 mb-sm-0 mb-3">
                            <div class="bookingDet">
                                <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Enter Details</p>
                                <p class="mb-0">Event Date</p>
                                <div class="input-group date-pick mb-3">
                                    <input type="text" class="form-control datepicker" id="date" placeholder="dd/mm/yyyy">
                                    <div class="input-group-append">
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                                <p class="mb-0">Event Starts</p>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="00 : 00" aria-label="" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <select class="form-control">
                                            <option value="am">AM</option>
                                            <option value="pm">PM</option>
                                        </select>
                                    </div>
                                </div>
                                <p class="mb-0">Occassion</p>
                                <select class="form-control">
                                    <option value="0">--Select--</option>
                                </select>
                                <hr class="my-4">
                                <button class="btn btn-primary btn-block"><i class="icon-check"></i> Confirm Booking</button>
                                <button class="btn btn-outline-primary btn-block"><i class="icon-heart"></i> Save Later</button>
                                <div class="other-info mt-3">
                                    <p>Primary Package</p>
                                    <h3>Dimond Veg</h3>
                                    <p>Price (Per Plate)</p>
                                    <h3><i class="icon-inr"></i> 470.00</h3>
                                    <p>No. of Items</p>
                                    <h3>36</h3>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="paymentDet h-100">
                                <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Enter Address</p>
                                <div class="row">
                                    <div class="col-6">
                                        <label for="homeD"><input type="radio" name="addrType" id="homeD" /> Home Delivery</label>
                                    </div>
                                    <div class="col-6">
                                        <label for="venueAddr"><input type="radio" name="addrType" id="venueAddr" /> Venue Address</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <select class="form-control mb-3">
                                            <option value="0">Pre-Saved Address</option>
                                        </select>
                                        <input type="text" placeholder="Street Name" class="form-control mb-3">
                                        <input type="text" placeholder="At/Post" class="form-control mb-3">
                                        <input type="text" placeholder="Pincode" class="form-control mb-3">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Plot No." class="form-control mb-3">
                                        <input type="text" placeholder="Landmark" class="form-control mb-3">
                                        <input type="text" placeholder="City" class="form-control mb-3">
                                        <select class="form-control mb-3">
                                            <option value="0">State</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="pt-3 text-center">
                                    <button class="btn btn-primary btn-sm px-4 py-2 h-auto">Edit</button>
                                    <button class="btn btn-success btn-sm px-4 py-2 h-auto">Save</button>
                                </div>
                                <p class="font-weight-bold m-0 mt-4 border-bottom pb-3 mb-3">Payment Detail</p>
                                <ul class="payment-detail">
                                    <li><span>Estimated Cost</span><strong><i class="icon-inr"></i> 1,05,000</strong></li>
                                    <li><span>Booking Amount</span><strong><i class="icon-inr"></i> 21,000</strong></li>
                                    <li><span>Remaining</span><strong><i class="icon-inr"></i> 84,000</strong></li>
                                </ul>
                                <div class="row mt-4">
                                    <div class="col-sm-4 pr-sm-0 mb-sm-0 mb-1"><button class="btn btn-primary btn-block"><i class="icon-file-pdf-o"></i> Download Menu</button></div>
                                    <div class="col-sm-4 pr-sm-0 mb-sm-0 mb-1"><button class="btn btn-primary btn-block"><i class="icon-check"></i> Confirm Booking</button></div>
                                    <div class="col-sm-4"><button class="btn btn-outline-primary btn-block"><i class="icon-heart"></i> Save Later</button></div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    
                </div>                
            </div>
            <div class="row mt-3">
                <div class="col-sm-4 pr-sm-0">
                    <div class="imgAdd">
                        <img src="imgs/scene.jpg" alt="">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="terms h-100">
                        <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Terms & Conditions</p>

                        <ul>
                            <li>
                                Remaining balance will be paid on event date.
                            </li>
                            <li>
                                If any due is exceeded over estimated cost due to additional order, it is under puragative for venue policy.
                            </li>
                            <li>
                                Venue policy is very friendly towards customer so any short of out of policy can't be entertained.
                            </li>
                            <li>
                                Within 36hrs of booking, booking can be cancelled & customer can get full refund but after 48hrs from the time of booking no refund will be made.
                            </li>
                            <li>
                                Evatril reserves the right of cacellation of the venue within 36hrs from the time of booking.
                            </li>
                            <li>
                                After 48hrs time of booking venue cancellation will not be possible for both Evatril & Venue Management.
                            </li>
                            <li>
                                Evatril reserves the right of venue booking confirmation within 24hrs.
                            </li>
                            <li>All venues booded 72hrs before the event can't be cancelled. If cancelled no refund will be made.</li>
                        </ul>
                        
                    </div>
                </div>
            </div>            
        </div>
    </section>
    <!-- Modal -->
    <div id="payModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p class="display-4">Payment Gateway</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div> 
<!-- Modal -->
<div id="cancelModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 text-center">
                        <p class="display-4">Booking Cancelled</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div> 
        
    <?php include 'includes/footer.php'; ?>
   
</body>
</html>