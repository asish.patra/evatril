
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>

    
    <section class="top-banner">
        <div id="bannerSlider" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#bannerSlider" data-slide-to="0" class="active"></li>
              <li data-target="#bannerSlider" data-slide-to="1"></li>
              <li data-target="#bannerSlider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src="imgs/banner1.jpg" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="imgs/banner2.jpg" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="imgs/banner3.jpg" alt="Third slide">
              </div>
            </div>
            <a class="carousel-control-prev" href="#bannerSlider" role="button" data-slide="prev">
                <i class="icon-keyboard_arrow_left"></i>
              <span class="sr-only">Prev</span>
            </a>
            <a class="carousel-control-next" href="#bannerSlider" role="button" data-slide="next">
                <i class="icon-keyboard_arrow_right"></i>
              <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="search-form text-center">
            <div class="container">
                <h1>Explore best Venue & Add value to your Happiness</h1>
                <form action="venue-list.php">
                    <div class="input-group">
                        <select id="venueType" class="form-control">
                            <option selected>Select City</option>
                        </select>
                        <select id="pincode" class="form-control">
                            <option selected>in Area / Pincode</option>
                        </select>
                        <select id="occasion" class="form-control">
                            <option selected>Occasion</option>
                        </select>
                    </div>
                    <button class="btn btn-primary px-5 mt-4 text-uppercase">Explore</button>
                </form>
                <p class="pt-3"><a href="services.php">Catering Service</a> | <a href="evatril-services.php">Decoration</a> | <a href="evatril-services.php">Photo / Videography</a> | <a href="evatril-services.php">Vehicle Booking</a> | <a href="evatril-services.php">Beautician</a> | <a href="evatril-services.php">Priest / Pandit</a> | <a href="evatril-services.php">DJ / Music</a> | <a href="evatril-services.php">Event Planner</a></p>
            </div>
        </div>
    </section>
    <section class="venue-categories d-sm-none ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="event-categories-list">
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-wedding-arch"></i></div>
                                <p>Grand/Destination Wedding</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-wedding-cake"></i></div>
                                <p>Birthday/Anniversary Party</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-diamond"></i></div>
                                <p>Engagement Day</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-cinema"></i></div>
                                <p>Cultural Ceremonry</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-student"></i></div>
                                <p>Graduation Day</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-property"></i></div>
                                <p>Home Inauguration</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-event"></i></div>
                                <p>Corporate Events</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-rip"></i></div>
                                <p>Funeral (Death Ceremoney)</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="services-list py-5">
        <div class="container">
            <h2 class="page-title text-center mb-5">Event Services</h2>
            <div class="row mb-sm-4">
                <div class="col-sm-4">
                    <a href="evatril-services.php" class="image-wrapper">
                        <img src="imgs/service-img1.jpg" alt="">
                        <p class="bottom text-right text-light">Venue Selection</p>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="evatril-services.php" class="image-wrapper">
                        <img src="imgs/service-img2.jpg" alt="">
                        <p class="top text-light">Vehicle Booking</p>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="evatril-services.php" class="image-wrapper">
                        <img src="imgs/service-img3.jpg" alt="">
                        <p class="top text-light">Choreography</p>
                    </a>
                </div>
            </div>
            <div class="row mb-sm-4 pt-1">
                <div class="col-sm-4">
                    <a href="services.php" class="image-wrapper">
                        <img src="imgs/service-img4.jpg" alt="">
                        <p class="top text-light">Menu Selection</p>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="evatril-services.php" class="image-wrapper">
                        <img src="imgs/service-img5.jpg" alt="">
                        <p class="bottom text-right text-light">DJ</p>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="evatril-services.php" class="image-wrapper">
                        <img src="imgs/service-img6.jpg" alt="">
                        <p class="bottom text-light">Beautician</p>
                    </a>
                </div>
            </div>
            <div class="row pt-1">
                <div class="col-sm-4">
                    <a href="evatril-services.php" class="image-wrapper">
                        <img src="imgs/service-img7.jpg" alt="">
                        <p class="center text-center text-light">Decoration</p>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="evatril-services.php" class="image-wrapper">
                        <img src="imgs/service-img8.jpg" alt="">
                        <p class="bottom text-right text-light">Photo/Videography</p>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="evatril-services.php" class="image-wrapper">
                        <img src="imgs/service-img9.jpg" alt="">
                        <p class="top text-light">Pandir/Priest</p>
                    </a>
                </div>
            </div>

        </div>
    </section>
    <section class="venue-categories py-sm-5">
        <div class="container">
            <h2 class="page-title text-center mb-2">Trending Venues</h2>
            <h4 class="page-sub-title text-center mb-5">Book your venue along with all other amenities & requirements for all event needs. We are here to serve you.</h4>
            <ul id="trending-venue-list">
                <li>
                    <div class="card">
                        <img src="imgs/venue-img1.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card">
                        <img src="imgs/venue-img2.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card">
                        <img src="imgs/venue-img3.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card">
                        <img src="imgs/venue-img3.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card">
                        <img src="imgs/venue-img3.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="row mt-4 d-none d-sm-block">
                <div class="col-sm-12">
                    <ul class="event-categories-list">
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-wedding-arch"></i></div>
                                <p>Grand/Destination Wedding</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-wedding-cake"></i></div>
                                <p>Birthday/Anniversary Party</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-diamond"></i></div>
                                <p>Engagement Day</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-cinema"></i></div>
                                <p>Cultural Ceremonry</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-student"></i></div>
                                <p>Graduation Day</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-property"></i></div>
                                <p>Home Inauguration</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-event"></i></div>
                                <p>Corporate Events</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon-wrapper"><i class="icon-rip"></i></div>
                                <p>Funeral (Death Ceremoney)</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="why-us py-5">
        <div class="container">
            <h2 class="page-title text-center mb-2">Why To Select Us</h2>
            <h4 class="page-sub-title text-center mb-5">Excel the creativity for your happiness & sorrow with evatril. Any event just remind Evatril. We will make your event memorable. A-Z event solution & the first event marketplace of India.</h4>
            <div class="row mb-sm-4">
                <div class="col-sm-4">
                    <div class="card">
                        <div class="icon-wrap"><i class="icon-eye"></i></div>
                        <div class="text-content">
                            <h3>Our Vision</h3>
                            <p>Successful events gives us smile making you happy gives joy continuing the same is our vision.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="icon-wrap"><i class="icon-eye"></i></div>
                        <div class="text-content">
                            <h3>Vision</h3>
                            <p>We will challenge our skills and abilities, and create an event management platform that’s strong and dependable.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="icon-wrap"><i class="icon-targeting"></i></div>
                        <div class="text-content">
                            <h3>Mission</h3>
                            <p>To be the leading Event Management Company in India, by meeting and exceeding the expectations.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-1">
                <div class="col-sm-4">
                    <div class="card">
                        <div class="icon-wrap"><i class="icon-truck"></i></div>
                        <div class="text-content">
                            <h3>Operations & Logistics</h3>
                            <p>Event operations & logistics is where all the time spent planning the event comes together in the execution of the event.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="icon-wrap"><i class="icon-trophy"></i></div>
                        <div class="text-content">
                            <h3>Drive Success</h3>
                            <p>Rely on a trusted & innovative technology partner who supports your team and helps you meet your goals.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="icon-wrap"><i class="icon-support"></i></div>
                        <div class="text-content">
                            <h3>24/7 Support</h3>
                            <p>we never sleep, and we have a dedicated support portal for logging and up-to-the-minute tracking for all support requests.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe-offers py-5 mb-3">
        <div class="container py-sm-5">
            <div class="row">
                <div class="col-sm-5 col-8">
                    <h2>SIGN UP TO GET SPECIAL OFFERS EVERY DAY</h2>
                    <h3>See venues and events for desired locations and bookmark your favorite places on the occasion</h3>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter your Phone Number" aria-label="" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn" type="button">Go</button>
                        </div>
                    </div>
                    <ul class="app-links">
                        <li><a href="#"><img src="imgs/app-store.png"></a></li>
                        <li><a href="#"><img src="imgs/play-store.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="our-blog py-5">
        <div class="container">
            <h2 class="page-title text-center mb-5">From Our Blog</h2>
            <div class="row mb-4">
                <div class="col-sm-6">
                    <a href="#" class="img-wrapper">
                        <img src="imgs/blog-img1.jpg" alt="">
                        <h3>Enhance Human Connections At Your Event</h3>
                        <span>11 April 2019</span>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#" class="img-wrapper">
                        <img src="imgs/blog-img2.jpg" alt="">
                        <h3>Enhance Human Connections At Your Event</h3>
                        <span>11 April 2019</span>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#" class="img-wrapper">
                        <img src="imgs/blog-img3.jpg" alt="">
                        <h3>Enhance Human Connections At Your Event</h3>
                        <span>11 April 2019</span>
                    </a>
                </div>
            </div>
            <div class="row pt-1">
                <div class="col-sm-3">
                    <a href="#" class="img-wrapper">
                        <img src="imgs/blog-img4.jpg" alt="">
                        <h3>Enhance Human Connections At Your Event</h3>
                        <span>11 April 2019</span>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#" class="img-wrapper">
                        <img src="imgs/blog-img5.jpg" alt="">
                        <h3>Enhance Human Connections At Your Event</h3>
                        <span>11 April 2019</span>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="img-wrapper">
                        <img src="imgs/blog-img6.jpg" alt="">
                        <h3>Enhance Human Connections At Your Event</h3>
                        <span>11 April 2019</span>
                    </a>
                </div>
            </div>
            <div class="text-center pt-5 pb-3"><button class="view-btn">View All <i class="icon-arrow-right"></i></button></div>
        </div>
    </section>
    

    <?php include 'includes/footer.php'; ?>
    
    <script src="js/lightslider.js"></script>
    <script type="text/javascript">
    
        $(document).ready(function() {
          $("#trending-venue-list").lightSlider({
            item:3,
            loop:false,
            speed:600,
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6,
                    }
                },
                {
                    breakpoint:480,
                    settings: {
                        item:2,
                        slideMove:1
                    }
                }
            ]
          }); 

          $(".event-categories-list").lightSlider({
            item:6,
            loop:false,
            speed:600,
            pager: false,
            prevHtml: "<i class='icon-keyboard_arrow_left'></i>",
            nextHtml: "<i class='icon-keyboard_arrow_right'></i>",
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6,
                    }
                },
                {
                    breakpoint:480,
                    settings: {
                        item:2,
                        slideMove:1
                    }
                }
            ]
          }); 
        });
    </script>
</body>
</html>