<?php include 'includes/doctype.php'; ?>

<body>

    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
        <div class="container bg-white py-3">
            <div class="row venue-links">
                <div class="col-3 pr-1">
                    <a href="#venue-profile">Venue Profile</a>
                </div>
                <div class="col-3 px-1">
                    <a href="#lawn-details">Hall/Lawn Details</a>
                </div>
                <div class="col-3 px-1">
                    <a href="#amenities">Amenities & FAQ</a>
                </div>
                <div class="col-3 pl-1">
                    <a href="#contact">Venue Contact</a>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <div class="venue-pic">
                        <div class="row mt-3">
                            <div class="col-sm-12">
                                <ul id="event-categories-list">
                                    <li data-thumb="imgs/venue-img1.jpg" data-src="imgs/venue-img1.jpg">
                                        <a href="#">
                                            <img src="imgs/venue-img1.jpg" alt="">
                                        </a>
                                    </li>
                                    <li data-thumb="imgs/venue-img2.jpg" data-src="imgs/venue-img2.jpg">
                                        <a href="#">
                                            <img src="imgs/venue-img2.jpg" alt="">
                                        </a>
                                    </li>
                                    <li data-thumb="imgs/venue-img3.jpg" data-src="imgs/venue-img3.jpg">
                                        <a href="#">
                                            <img src="imgs/venue-img3.jpg" alt="">
                                        </a>
                                    </li>
                                    <li data-thumb="imgs/venue-img4.jpg" data-src="imgs/venue-img4.jpg">
                                        <a href="#">
                                            <img src="imgs/venue-img4.jpg" alt="">
                                        </a>
                                    </li>
                                    <li data-thumb="imgs/venue-img1.jpg" data-src="imgs/venue-img1.jpg">
                                        <a href="#">
                                            <img src="imgs/venue-img1.jpg" alt="">
                                        </a>
                                    </li>
                                    <li data-thumb="imgs/venue-img2.jpg" data-src="imgs/venue-img2.jpg">
                                        <a href="#">
                                            <img src="imgs/venue-img2.jpg" alt="">
                                        </a>
                                    </li>
                                    <li data-thumb="imgs/venue-img3.jpg" data-src="imgs/venue-img3.jpg">
                                        <a href="#">
                                            <img src="imgs/venue-img3.jpg" alt="">
                                        </a>
                                    </li>
                                    <li data-thumb="imgs/venue-img4.jpg" data-src="imgs/venue-img4.jpg">
                                        <a href="#">
                                            <img src="imgs/venue-img4.jpg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-7 col-sm-6">
                    <div class="row">

                        <div class="col-sm-12 venue-detl">

                            <ul>
                                <li>
                                    <h1>Mayfair Lagoon</h1><small>Ajad Road, Jayadev Vihar, Bhubaneswar-08</small>
                                </li>

                                <li>
                                    <p class="rattings-btn">
                                        <strong><i class="icon-star mr-1"></i> 2.5</strong>
                                        <a href="#">4 Reviews</a>
                                    </p>
                                    <!-- <p class="rattings mb-0">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <label>2.5 / 5</label>
                                        <a href="#">4 Reviews</a>
                                    </p> -->

                                </li>
                                <li class="d-flex mb-2">
                                    <a href="#" class="btn btn-outline-primary btn-sm"><i class="icon-pin1"></i><span>View on Map</span></a>
                                    <a href="#" class="btn btn-outline-primary btn-sm ml-3"><i class="icon-phone-call"></i><span>Phone Number of Venue</span></a>
                                </li>
                                <!-- <li>
                                    <p><i class="icon-catering"></i>Total / <i class="icon-lunch1"></i>Per plate</p>
                                    <span for=""><i class="icon-inr"></i>2500 - <i class="icon-inr"></i>3500</span>
                                </li> -->

                                <!-- <li><strong>Number of Halls</strong><label>10</label></li>
                                <li><strong>Number of lawns</strong><label>4</label></li> -->
                            </ul>
                            <div class="row mb-4">
                                <div class="col-sm-6 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-wedding-cake text-secondary"></i>
                                    <strong>Food Allowed  </strong><label>Non-Veg</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-air-conditioner text-success"></i>
                                        <strong>A/C  </strong> <label><i class="icon-check"></i> Yes</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-6 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-cinema text-info"></i>
                                        <strong>Venue Type</strong> <label>Hotel</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-lunch1 text-warning"></i>
                                    <strong>Per Plate  </strong><label><i class="icon-inr"></i> 2500 - <i class="icon-inr"></i> 3500</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-property text-danger"></i>
                                        <strong>Number of Halls</strong> <label>10</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-6 pr-0">
                                    <div class="amenities">
                                        <i class="icon-praying text-primary"></i>
                                        <strong>Number of Lawns</strong><label>4</label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3" id="venue-profile">
                <div class="col-sm-12">
                    <div class="hall-detl">
                        <strong>Hall Description</strong>
                        <p class="mt-2">
                            Excel the creativity for your happiness & sorrow with evatril. Any event just remind Evatril. We wil <a href="#">Learn more...</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row mt-3" id="lawn-details">
                <div class="col-sm-12">
                    <div class="myCard">
                        <div class="card-header">Sub-Hall/Lawn Details</div>
                        <div class="card-body">
                            <div class="hall-list mb-3">
                                <div class="hall-imgs">
                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img src="imgs/venue-img2.jpg" class="d-block w-100" alt="...">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="imgs/venue-img3.jpg" class="d-block w-100" alt="...">
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="hall-info px-lg-4">
                                    <h4>Mayfair Lagoon</h4>
                                    <ul class="payment-detail">
                                        <li>
                                            <span>Theatre style seating</span>
                                            <strong>300</strong>
                                        </li>
                                        <li>
                                            <span>Round table style seating</span>
                                            <strong>450</strong>
                                        </li>
                                        <li>
                                            <span>Floating</span>
                                            <strong>1500</strong>
                                        </li>
                                        <li>
                                            <span>Area (Sqft)</span>
                                            <strong>4500</strong>
                                        </li>
                                        <li>
                                            <span>Floor/Level</span>
                                            <strong>6th</strong>
                                        </li>
                                        <li>
                                            <span>A/C</span>
                                            <strong class="text-success"><i class="icon-check h4 align-middle"></i> Yes</strong>
                                        </li>
                                        <li>
                                            <span>Non A/C</span>
                                            <strong class="text-danger"><i class="icon-close h6 align-middle"></i> No</strong>
                                        </li>
                                        <li>
                                            <span>Common Area AC</span>
                                            <strong class="text-danger"><i class="icon-close h6 align-middle"></i> No</strong>
                                        </li>
                                    </ul>
                                </div>
                                <div class="hall-booking">
                                    <div class="card hall-availability">
                                        <div class="card-body">
                                            <div class="row align-items-center">
                                                <div class="col-sm-12 mb-3">
                                                    <div class="hall-dt">
                                                        <label for="">Select Date</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datepicker" id="date" placeholder="dd/mm/yyyy">
                                                            <div class="input-group-append">
                                                                <i class="icon-calendar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="hall-timing">
                                                        <div class="card timing-card available">
                                                            <div class="card-body">
                                                                <label for="morningSlot" class="mb-0">
                                                                    <p>Morning Slot (7.00 A.M - 11.30 A.M)</p>
                                                                    <strong><i class="icon-check"></i> Available</strong>
                                                                    <input type="checkbox" id="morningSlot" name="example1">
                                                                    <label for="morningSlot"></label>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="card timing-card booked">
                                                            <div class="card-body">
                                                                <p>Launch Slot (1.00 P.M - 4.30 P.M)</p>
                                                                <strong><i class="icon-ban"></i> Booked</strong>
                                                            </div>
                                                        </div>
                                                        <div class="card timing-card booked">
                                                            <div class="card-body">
                                                                <p>Evening Slot (5.00 P.M - 7.00 P.M)</p>
                                                                <strong><i class="icon-ban"></i> Booked</strong>
                                                            </div>
                                                        </div>

                                                        <div class="card timing-card booked">
                                                            <div class="card-body">
                                                                <p>Dinner Slot (7.00 P.M - 11.30 P.M)</p>
                                                                <strong><i class="icon-ban"></i> Booked</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 mt-3">
                                                    <button class="btn btn-outline-primary btn-block text-uppercase" id="bookBtn" data-toggle="modal" data-target="#bookingError">Book Now</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr class="my-3">

                            <div class="hall-list mb-3">
                                <div class="hall-imgs">
                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img src="imgs/venue-img2.jpg" class="d-block w-100" alt="...">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="imgs/venue-img3.jpg" class="d-block w-100" alt="...">
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="hall-info px-lg-4">
                                    <h4>Mayfair Lagoon</h4>
                                    <ul class="payment-detail">
                                        <li>
                                            <span>Theatre style seating</span>
                                            <strong>300</strong>
                                        </li>
                                        <li>
                                            <span>Round table style seating</span>
                                            <strong>450</strong>
                                        </li>
                                        <li>
                                            <span>Floating</span>
                                            <strong>1500</strong>
                                        </li>
                                        <li>
                                            <span>Area (Sqft)</span>
                                            <strong>4500</strong>
                                        </li>
                                        <li>
                                            <span>Floor/Level</span>
                                            <strong>6th</strong>
                                        </li>
                                        <li>
                                            <span>A/C</span>
                                            <strong class="text-success"><i class="icon-check h4 align-middle"></i> Yes</strong>
                                        </li>
                                        <li>
                                            <span>Non A/C</span>
                                            <strong class="text-danger"><i class="icon-close h6 align-middle"></i> No</strong>
                                        </li>
                                        <li>
                                            <span>Common Area AC</span>
                                            <strong class="text-danger"><i class="icon-close h6 align-middle"></i> No</strong>
                                        </li>
                                    </ul>
                                </div>
                                <div class="hall-booking">
                                    <div class="card hall-availability">
                                        <div class="card-body">
                                            <div class="row align-items-center">
                                                <div class="col-sm-12 mb-3">
                                                    <div class="hall-dt">
                                                        <label for="">Select Date</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datepicker" id="date" placeholder="dd/mm/yyyy">
                                                            <div class="input-group-append">
                                                                <i class="icon-calendar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="hall-timing">
                                                        <div class="card timing-card available">
                                                            <div class="card-body">
                                                                <label for="morningSlot" class="mb-0">
                                                                    <p>Morning Slot (7.00 A.M - 11.30 A.M)</p>
                                                                    <strong><i class="icon-check"></i> Available</strong>
                                                                    <input type="checkbox" id="morningSlot" name="example1">
                                                                    <label for="morningSlot"></label>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="card timing-card booked">
                                                            <div class="card-body">
                                                                <p>Launch Slot (1.00 P.M - 4.30 P.M)</p>
                                                                <strong><i class="icon-ban"></i> Booked</strong>
                                                            </div>
                                                        </div>

                                                        <div class="card timing-card booked">
                                                            <div class="card-body">
                                                                <p>Evening Slot (5.00 P.M - 7.00 P.M)</p>
                                                                <strong><i class="icon-ban"></i> Booked</strong>
                                                            </div>
                                                        </div>
                                                        <div class="card timing-card booked">
                                                            <div class="card-body">
                                                                <p>Dinner Slot (7.00 P.M - 11.30 P.M)</p>
                                                                <strong><i class="icon-ban"></i> Booked</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 mt-3">
                                                    <button class="btn btn-outline-primary btn-block text-uppercase" id="bookBtn" data-toggle="modal" data-target="#bookingError">Book Now</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>






                            <div class="row d-none">
                                <div class="col-sm-4 pr-0">
                                    <div class="sub-hall-img">
                                        <img src="imgs/venue-img2.jpg">
                                        <img src="imgs/venue-img3.jpg" class="mt-3">

                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="sub-hall-detl">
                                        <div class="row">
                                            <div class="col">
                                                <label>Hall Name <strong>Mayfair Lagoon</strong></label>
                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col">
                                                <label>Theatre style seating<strong>1200</strong></label>

                                            </div>
                                            <div class="col px-0">
                                                <label>Round table style seating<strong>450</strong></label>

                                            </div>
                                            <div class="col">
                                                <label>Floating<strong>1500</strong></label>

                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col pr-0">
                                                <label>Area (Sqft)<strong>4500</strong></label>

                                            </div>
                                            <div class="col">
                                                <label>Floor/Level<strong>6th</strong></label>

                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col">
                                                <label>Hall A/c<strong><i class="icon-check"></i> Yes</strong></label>

                                            </div>
                                            <div class="col px-0">
                                                <label>Dining A/c<strong><i class="icon-check"></i> Yes</strong></label>

                                            </div>
                                            <div class="col">
                                                <label>Common Area AC<strong><i class="icon-ban"></i> No</strong></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card d-none mt-3 hall-availability">
                                <div class="card-header">Check Availability</div>
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-sm-3 pr-0">

                                            <div class="hall-dt">
                                                <label for="">Select Date</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker" id="date" placeholder="dd/mm/yyyy">
                                                    <div class="input-group-append">
                                                        <i class="icon-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="d-flex hall-timing">
                                                <div class="card timing-card col available">
                                                    <div class="card-body">
                                                        <label for="morningSlot">
                                                            <p>Morning Slot</p>
                                                            <span>7.00 A.M - 11.30 A.M</span>
                                                            <strong><i class="icon-check"></i> Available</strong>
                                                            <input type="checkbox" id="morningSlot" name="example1">
                                                            <label for="morningSlot"></label>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="card timing-card col booked">
                                                    <div class="card-body">
                                                        <p>Launch Slot</p>
                                                        <span>1.00 P.M - 4.30 P.M</span>
                                                        <strong><i class="icon-ban"></i> Booked</strong>
                                                    </div>
                                                </div>

                                                <div class="card timing-card col booked">
                                                    <div class="card-body">
                                                        <p>Dinner Slot</p>
                                                        <span>7.00 P.M - 11.30 P.M</span>
                                                        <strong><i class="icon-ban"></i> Booked</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 pl-0">
                                            <button class="btn btn-outline-primary text-uppercase w-100" id="bookBtn" data-toggle="modal" data-target="#bookingError">Book Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4 d-none">
                            <div class="row d-none">
                                <div class="col-sm-4 pr-0">
                                    <div class="sub-hall-img">
                                        <img src="imgs/venue-img2.jpg">
                                        <img src="imgs/venue-img3.jpg" class="mt-3">

                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="sub-hall-detl">
                                        <div class="row">
                                            <div class="col">
                                                <label>Hall Name <strong>Mayfair Lagoon</strong></label>
                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col">
                                                <label>Theatre style seating<strong>1200</strong></label>

                                            </div>
                                            <div class="col px-0">
                                                <label>Round table style seating<strong>450</strong></label>

                                            </div>
                                            <div class="col">
                                                <label>Floating<strong>1500</strong></label>

                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col pr-0">
                                                <label>Area (Sqft)<strong>4500</strong></label>

                                            </div>
                                            <div class="col">
                                                <label>Floor/Level<strong>6th</strong></label>

                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col">
                                                <label>Hall A/c<strong><i class="icon-check"></i> Yes</strong></label>

                                            </div>
                                            <div class="col px-0">
                                                <label>Dining A/c<strong><i class="icon-check"></i> Yes</strong></label>

                                            </div>
                                            <div class="col">
                                                <label>Common Area AC<strong><i class="icon-ban"></i> No</strong></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card d-none mt-3 hall-availability">
                                <div class="card-header">Check Availability</div>
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-sm-3 pr-0">

                                            <div class="hall-dt">
                                                <label for="">Select Date</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker" id="date" placeholder="dd/mm/yyyy">
                                                    <div class="input-group-append">
                                                        <i class="icon-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="d-flex hall-timing">
                                                <div class="card timing-card col available">
                                                    <div class="card-body">
                                                        <label for="morningSlot1">
                                                            <p>Morning Slot</p>
                                                            <span>7.00 A.M - 11.30 A.M</span>
                                                            <strong><i class="icon-check"></i> Available</strong>
                                                            <input type="checkbox" id="morningSlot1" name="example1">
                                                            <label for="morningSlot1"></label>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="card timing-card col booked">
                                                    <div class="card-body">
                                                        <p>Launch Slot</p>
                                                        <span>1.00 P.M - 4.30 P.M</span>
                                                        <strong><i class="icon-ban"></i> Booked</strong>
                                                    </div>
                                                </div>

                                                <div class="card timing-card col booked">
                                                    <div class="card-body">
                                                        <p>Dinner Slot</p>
                                                        <span>7.00 P.M - 11.30 P.M</span>
                                                        <strong><i class="icon-ban"></i> Booked</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 pl-0">
                                            <button class="btn btn-outline-primary text-uppercase w-100" id="bookBtn" data-toggle="modal" data-target="#bookingError">Book Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

            </div>
            <div class="row mt-3">
                <div class="col-sm-12">
                </div>
            </div>
            <div class="row mt-3" id="amenities">
                <div class="col-sm-6">

                    <div class="card myCard amenties">
                        <div class="card-header">Amenities</div>
                        <div class="card-body">
                            <ul class="amenities-list">
                                <li>
                                    <a href="javascript: void(0);"><i class="icon-cinema"></i> Hall capacity <button class="list-toggle"><span class="icon-minus"></span><span class="icon-plus"></span></button></a>
                                    <ul class="items">
                                        <li>
                                            <strong>Theatre style sitting</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Round style sitting</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Floating Capacity</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript: void(0);"><i class="icon-dinning-table"></i> Dining Capacity <button class="list-toggle"><span class="icon-minus"></span><span class="icon-plus"></span></button></a>
                                    <ul class="items">
                                        <li>
                                            <strong>Sitting Capacity</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Floating / Buffet</strong>
                                            <span><i class="icon-close"></i> No</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript: void(0);"><i class="icon-wedding-cake"></i> Food & Liqour <button class="list-toggle"><span class="icon-minus"></span><span class="icon-plus"></span></button></a>
                                    <ul class="items">
                                        <li>
                                            <strong>Food</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Beverage</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Liqour</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript: void(0);"><i class="icon-wedding-arch"></i> Venue Access <button class="list-toggle"><span class="icon-minus"></span><span class="icon-plus"></span></button></a>
                                    <ul class="items">
                                        <li>
                                            <strong>Multiple Entrance</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Rest Room</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Washroom</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript: void(0);"><i class="icon-event1"></i> Decor & Ritual <button class="list-toggle"><span class="icon-minus"></span><span class="icon-plus"></span></button></a>
                                    <ul class="items">
                                        <li>
                                            <strong>Decoration</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Priest</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript: void(0);"><i class="icon-film"></i> Audio & Visuals <button class="list-toggle"><span class="icon-minus"></span><span class="icon-plus"></span></button></a>
                                    <ul class="items">
                                        <li>
                                            <strong>Still Photography</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Video Shooting</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Music & Choreography</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript: void(0);"><i class="icon-air-conditioner"></i> Air Conditioning <button class="list-toggle"><span class="icon-minus"></span><span class="icon-plus"></span></button></a>
                                    <ul class="items">
                                        <li>
                                            <strong>Ceiling Fan</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Stand Fan</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Split A/c</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>Tower A/c</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript: void(0);"><i class="icon-car"></i> Parking <button class="list-toggle"><span class="icon-minus"></span><span class="icon-plus"></span></button></a>
                                    <ul class="items">
                                        <li>
                                            <strong>2 Wheeler Parking</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                        <li>
                                            <strong>4 Wheeler Parking</strong>
                                            <span><i class="icon-check"></i> Yes</span>
                                            <label><i class="icon-inr"></i> 125.00</label>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 pl-0">
                    <div class="card myCard faq">
                        <div class="card-header">Faq</div>
                        <div class="card-body">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                            1. How to book a hall?
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                        <div class="card-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                            2. Is parking available?
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                            3. In which season hall price hikes?
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse4">
                                            4. Is there any arrangement of decoration & flower?
                                        </a>
                                    </div>
                                    <div id="collapse4" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse5">
                                            5. When we can avail discount?
                                        </a>
                                    </div>
                                    <div id="collapse5" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapse6">
                                            6. Is there any rest room inside the hall?
                                        </a>
                                    </div>
                                    <div id="collapse6" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="mt-3 card myCard" id="contact">
                <div class="card-header">Venue Contact</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-8 mb-sm-0 mb-5">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29926.08394810133!2d85.80673220310041!3d20.35151069304649!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a1909144e9eae2f%3A0x23d9c04c9efb24bb!2sRJ%20Lawns%20-%20Marriage%20Mandap!5e0!3m2!1sen!2sin!4v1585699794755!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                            <h4 class="mt-5 mb-0">10 Reviews</h4>
                            <div class="footer-rattings">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star_half"></i>
                                <i class="icon-star_border"></i>
                                <i class="icon-star_border"></i>
                                <label>2.5 / 5</label>
                                <a href="#">4 Reviews</a>
                            </div>
                            <hr>
                            <div class="review-content-list">
                                <div class="person py-2">
                                    <label>R</label>
                                    <div>
                                        <p class="mb-0"><strong>Rahul Ku. Sharma</strong>, <small>22 Mar 2020</small></p>
                                        <p class="mb-0">"Quality of food was good and service is also nice."</p>
                                    </div>
                                </div>
                                <div class="person py-2">
                                    <label>R</label>
                                    <div>
                                        <p class="mb-0"><strong>Rahul Ku. Sharma</strong>, <small>22 Mar 2020</small></p>
                                        <p class="mb-0">"Quality of food was good and service is also nice."</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <h3>RJ Lawns - Marriage Mandap</h3>
                            <ul class="contact-list">
                                <li>
                                    <i class="icon-location"></i>
                                    440, Swarnapuri Road, Patia, Bhubaneswar, Odisha 751024
                                </li>
                                <li>
                                    <i class="icon-globe"></i>
                                    rjlawns.com
                                </li>
                                <li>
                                    <i class="icon-phone-call"></i>
                                    079927 32627
                                </li>
                                <li>
                                    <i class="icon-cart2"></i>
                                    <span class="badge badge-pill badge-success">Open</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <h3 class="mt-5 mb-3">Similar Venues</h3>
            <ul id="trending-venue-list">
                <li>
                    <div class="card">
                        <img src="imgs/venue-img1.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card">
                        <img src="imgs/venue-img2.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card">
                        <img src="imgs/venue-img3.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card">
                        <img src="imgs/venue-img3.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card">
                        <img src="imgs/venue-img3.jpg" alt="">
                        <div class="text-content">
                            <a href="venue-details.php">De Grandeur Hotel And Banquets</a>
                            <p>De Grandeur Hotel and Banquets, Thane West, Mumbai was previ...</p>
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <div class="reviews-icon">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <span>4 reviews</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <i class="icon-rupee"></i> 20 / Plate
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>






    </section>
    <!-- Modal -->
    <div id="bookingError" class="modal fade bookModal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="row">
                        <div class="col-3">
                            <img src="imgs/error.png" alt="">
                        </div>
                        <div class="col-9">
                            <div class="hallCheckError">
                                <h3>For Wedding Occassion</h3>
                                <ul>
                                    <li class="text-danger">Please Select one presentation hall's slot & one dining hall's slot as per Availability</li>
                                </ul>
                                <button type="button" class="btn btn-danger text-uppercase" data-dismiss="modal">Select Hall's Slot</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="text-center">
                        <p class="text-muted">In case of any doubt please contact us</p>
                        <a href="tel:+91 7008772208"><i class="icon-phone-call mr-2"></i>+91 7008772208</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div id="bookingOk" class="modal fade bookModal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <div class="row">
                        <div class="col-3">
                            <img src="imgs/ok.png" alt="">
                        </div>
                        <div class="col-9">
                            <div class="hallCheckOk">
                                <h3>Venue Rental Per Occassion</h3>
                                <label for="">₹ 40,000</label>
                                <span class="border d-block p-3 mb-3">
                                    <p> <i class="icon-info_outline"></i> 20% Accepted as Booking Advance</p>
                                    <strong class="m-0 display-4">₹ 8,000</strong>
                                </span>
                                <button class="btn btn-primary text-uppercase w-50" onclick="window.location.href = 'book-menu-only.php';">Make Payment</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="text-center">
                        <p class="text-muted">In case of any doubt please contact us</p>
                        <a href="tel:+91 7008772208"><i class="icon-phone-call mr-2"></i>+91 7008772208</a>
                    </div>

                </div>

            </div>

        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
    <script src="js/lightslider.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#event-categories-list').lightSlider({
                gallery: true,
                item: 1,
                loop: true,
                thumbItem: 5,
                slideMargin: 0,
                enableDrag: false,
                currentPagerPosition: 'left',
                onSliderLoad: function(el) {
                    el.lightGallery({
                        selector: '#event-categories-list .lslide'
                    });
                }
            });
            $("#trending-venue-list").lightSlider({
                item: 3,
                loop: false,
                speed: 600,
                responsive: [{
                        breakpoint: 800,
                        settings: {
                            item: 3,
                            slideMove: 1,
                            slideMargin: 6,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            item: 2,
                            slideMove: 1
                        }
                    }
                ]
            });
        });
        $(document).ready(function() {
            $('.list-toggle').click(function() {
                $(this).parent().next('.items').slideToggle();
                $(this).toggleClass('closed');
            });


            $('input[type="checkbox"]').click(function() {
                if ($(this).prop("checked") == true) {
                    $("#bookBtn").attr('data-target', '#bookingOk');
                } else if ($(this).prop("checked") == false) {
                    $("#bookBtn").attr('data-target', '#bookingError');
                }
            });
        });
    </script>
</body>

</html>