
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-4 pr-sm-0">
            <div class="whiteBox p-3 d-flex">
                <img src="imgs/profile.png" class="mr-3" alt="" width="45" height="45">
                <h4><small class="d-block">Hello,</small>Himalay Pagada</h4>
            </div>
            <?php include 'includes/profile-sidelinks.php'; ?>
            
          </div>
          <div class="col-lg-9 col-sm-8">
            <h3>User Profile</h3>
            <div class="whiteBox userProfile mt-3 p-3">
              <div class="row">
                <div class="col-sm-8">
                    <p class="nheading">Personal Information <a href="#">Edit</a></p>
                    <div class="row">
                      <div class="col-6 pr-0">
                        <input type="text" class="form-control" value="Himalaya">
                      </div>
                      <div class="col-6">
                        <input type="text" class="form-control" value="Pagada">
                      </div>
                      <div class="col-sm-12 mt-5">
                        <p class="nheading">Your Gender <a href="#">Edit</a></p>
                        <div class="row">
                          <div class="col-2 pr-0">
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio" name="example1" value="customEx">
                              <label class="custom-control-label" for="customRadio">Male</label>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio1" name="example2" value="customEx1">
                              <label class="custom-control-label" for="customRadio1">Female</label>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-5">
                      <p class="nheading">Email Address <a href="#">Edit</a> <a href="#">Change Password</a></p>
                      <div class="row">
                        <div class="col-8">
                          <input type="text" class="form-control" value="himalay.pagada@gmail.com">
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-12 mt-5">
                    <p class="nheading">Mobile Number <a href="#">Edit</a></p>
                    <div class="row">
                      <div class="col-8">
                        <input type="text" class="form-control" value="8917220478">
                      </div>
                    </div>
                </div>
                    </div>
                </div>
                <div class="col-sm-4 mt-sm-0 mt-4">
                  <img src="imgs/user.jpg" class="w-100" alt="">
                </div>
              </div>
            
              
            </div>
          </div>
        </div>
      </div>

    </section>

    <?php include 'includes/footer.php'; ?>
   
</body>
</html>