
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="menuPackages">
                    <h4 class="py-3">Select Menu Package</h4>
                    <ul>
                        <li>
                            <a href="javascript:void(0);" class="active">
                                <strong><span class="icon-catering"></span> Diamond Veg</strong>
                                <span class="rate">₹ 3000 </span><small>40 Items</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <strong><span class="icon-catering"></span> Gold Veg</strong>
                                <span class="rate">₹ 2500 </span><small>32 Items</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <strong><span class="icon-catering"></span> Silver Veg</strong>
                                <span class="rate">₹ 2200 </span><small>18 Items</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>
                        
                        <li>
                            <a href="javascript:void(0);">
                                <strong><span class="icon-catering"></span> Diamond Non-Veg</strong>
                                <span class="rate">₹ 4000 </span><small>50 Items</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <strong><span class="icon-catering"></span> Gold Non-Veg</strong>
                                <span class="rate">₹ 3500 </span><small>32 Items</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>
                        <li>
                            
                            <a href="javascript:void(0);">
                                <strong><span class="icon-catering"></span> Silver Non-Veg</strong>
                                <span class="rate">₹ 3400 </span><small>22 Items</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>                                                                                                
                    </ul>
                </div>
                <div class="card ads-card mt-3 mb-sm-0 mb-3">
                    <img src="imgs/ads.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-8 pl-0">
            
                <div class="pkgDetails">
                    <span class="rSymbol nveg"></span>
                    <p class="mb-4 pb-3 h3 border-bottom font-weight-normal">Dimond Veg</p>                    
                    <div class="row pr-3">
                        <div class="col-sm-4 p-0 pl-3 mb-2 mb-lg-0"><img src="imgs/food-img1.jpg" alt=""></div>
                        <div class="col-sm-4 p-0 pl-3 mb-2 mb-lg-0"><img src="imgs/food-img2.jpg" alt=""></div>
                        <div class="col-sm-4 p-0 pl-3"><img src="imgs/food-img3.jpg" alt=""></div>
                    </div>
                </div>

                <div class="row mt-3">
                        <div class="col-sm-6 pr-0 mb-sm-0 mb-3">
                            <div class="package-info">
                                <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Package Information</p>
                                <div class="row mt-4">
                                    <div class="col-sm-12 mb-2"><button class="btn btn-primary text-uppercase btn-block" data-toggle="modal" data-target="#advanceModal">Select</button></div>
                                    <div class="col-sm-12"><a href="addon.php" class="btn btn-outline-primary btn-block"><i class="icon-plus"></i> ADD ON</a></div>
                                </div>
                                <hr>
                                <div class="row-ln">
                                    <i class="icon-catering"></i>
                                    <label for="">Minimum Order</label>
                                    <strong class="col-red">60</strong>
                                </div>
                                <div class="row-ln">
                                    <i class="icon-lunch1"></i>
                                    <label>Price <small>(Per Plate)</small></label>
                                    <strong><span class="icon-inr"></span> 2,500</strong>
                                </div>
                                <div class="row-ln">
                                    <i class="icon-dinning-table"></i>
                                    <label>Number of Items</label>
                                    <strong>32</strong>
                                </div>
                            </div> 
                        </div>
                        <div class="col-sm-6">
                            <div class="paymentDet h-100">
                                <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Package Item Details</p>
                                <div class="pkgItemsDet">
                                    <div><strong>Appetizer (Starter)</strong><small>Orange Juice</small><small>Beer</small><small> Vodka</small><small>Rum</small></div>
                                    <div><strong>Main Course</strong></small><small>Corn Crisp</small><small>Veg Manchurian</small><small>Panner Pakoda</small><small> Gupchup</small><small>Chat</small></div>
                                    <div><strong>Dessert</strong><small>Payes, Ice-cream</small><small>Rasgolla</small><small>Jalebi</small><small> Malpua</small><small>Khirsagar</small></div>
                                    
                                    <div class="row mt-4">
                                        <div class="col-sm-6 pr-lg-0"><button class="btn btn-primary text-uppercase btn-block" data-toggle="modal" data-target="#advanceModal">Select</button></div>
                                        <div class="col-sm-6"><a href="addon.php" class="btn btn-outline-primary btn-block"><i class="icon-plus"></i> ADD ON</a></div>
                                    </div>
                                </div>                                 
                            </div>
                        </div>
                    </div>


        </div>
                    
    </div>
    </section>
    
    <?php include 'includes/footer.php'; ?>


<!-- Advance Modal -->
<div class="modal fade" id="advanceModal" tabindex="-1" role="dialog" aria-labelledby="advanceModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="advanceModalLabel">Payment Informations</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <?php include 'includes/payment-confirmation.php'; ?>
        </div>
        </div>
    </div>
</div>

</body>
</html>