
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="menuPackages">
                    <h4 class="py-3">Select Decore Package</h4>
                    <ul>
                        <li>
                            <a href="javascript:void(0);" class="active">
                                <strong><span class="icon-christmas"></span> Standard Package</strong>
                                <span class="rate">₹ 6000 </span><small>Social Events</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>                                                                                               
                        <li>
                            <a href="javascript:void(0);" class="active">
                                <strong><span class="icon-christmas"></span> Premium Package</strong>
                                <span class="rate">₹ 10,000 </span><small>Social Events</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>                                                                                               
                        <li>
                            <a href="javascript:void(0);" class="active">
                                <strong><span class="icon-christmas"></span> Standard Package</strong>
                                <span class="rate">₹ 7000 </span><small>Corporate Events</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>                                                                                               
                        <li>
                            <a href="javascript:void(0);" class="active">
                                <strong><span class="icon-christmas"></span> Quick Booking</strong>
                                <span class="rate">₹ 599 </span><small>Any Event</small>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </li>                                                                                               
                    </ul>
                </div>
                <div class="card ads-card mt-3 mb-sm-0 mb-3">
                    <img src="imgs/ads.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-8 pl-0">
            
                <div class="pkgDetails">
                    <p class="mb-4 pb-3 h3 border-bottom font-weight-normal">Decore Package</p>                    
                    <div class="row pr-3">
                        <div class="col-sm-4 p-0 pl-3 mb-2 mb-lg-0"><img src="imgs/venue-decor.jpg" alt=""></div>
                        <div class="col-sm-4 p-0 pl-3 mb-2 mb-lg-0"><img src="imgs/venue-decor.jpg" alt=""></div>
                        <div class="col-sm-4 p-0 pl-3"><img src="imgs/venue-decor.jpg" alt=""></div>
                    </div>
                </div>

                <div class="row mt-3">
                        <div class="col-sm-6 pr-0 mb-sm-0 mb-3">
                            <div class="package-info">
                                <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Package Information</p>
                                <div class="row mt-4">
                                    <div class="col-sm-12 mb-2"><button class="btn btn-primary text-uppercase btn-block" data-toggle="modal" data-target="#advanceModal">Select</button></div>
                                    <div class="col-sm-12"><a href="evatril-addon.php" class="btn btn-outline-primary btn-block"><i class="icon-plus"></i> ADD ON</a></div>
                                </div>
                                <hr>
                                <div class="row-ln">
                                    <i class="icon-christmas"></i>
                                    <label for="">Standard Decor + Social Event</label>
                                    <strong class="col-red"><span class="icon-inr"></span>  6,000</strong>
                                </div>
                                <ul class="package-detail-list any-detail">
                                    <li>
                                        <i>1</i>
                                        Any Detail 1
                                    </li>
                                    <li>
                                        <i>2</i>
                                        Any Detail 2
                                    </li>
                                </ul>
                            </div> 
                        </div>
                        <div class="col-sm-6">
                            <div class="paymentDet h-100">
                                <p class="font-weight-bold m-0 border-bottom pb-3 mb-3">Package Details</p>
                                <div class="pkgItemsDet">
                                    <ul class="package-detail-list">
                                        <li>
                                            <i class="icon-box"></i>
                                            Birthday, Anniversary, Small Party Packages
                                        </li>
                                        <li>
                                            <i class="icon-box"></i>
                                            Birthday, Anniversary, Small Party Packages
                                        </li>
                                        <li>
                                            <i class="icon-box"></i>
                                            Birthday, Anniversary, Small Party Packages
                                        </li>
                                        <li>
                                            <i class="icon-box"></i>
                                            Birthday, Anniversary, Small Party Packages
                                        </li>
                                    </ul>
                                    
                                    <div class="row mt-4">
                                        <div class="col-sm-6 pr-lg-0"><button class="btn btn-primary text-uppercase btn-block" data-toggle="modal" data-target="#advanceModal">Select</button></div>
                                        <div class="col-sm-6"><a href="evatril-addon.php" class="btn btn-outline-primary btn-block"><i class="icon-plus"></i> ADD ON</a></div>
                                    </div>
                                </div>                                 
                            </div>
                        </div>
                    </div>


        </div>
                    
    </div>
    </section>
    
    <?php include 'includes/footer.php'; ?>


<!-- Advance Modal -->
<div class="modal fade" id="advanceModal" tabindex="-1" role="dialog" aria-labelledby="advanceModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="advanceModalLabel">Payment Informations</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="estimated-cost-wrapper pt-0">
                <div class="amout">
                    <p>Primary Package: <strong><br><i class="icon-inr"></i> 6,000</strong></p>
                    <p>ADDON Price: <strong><i class="icon-inr"></i> 6,998</strong></p>
                    <p>Total : <strong class="text-primary"><i class="icon-inr"></i> 12,998</strong></p>
                </div>
                <div class="advance">
                    <p>20% of the Estimated Cost in Advance</p>
                    <p class="mb-0"><small>Booking Amount:</small> <br><strong><i class="icon-inr"></i> 1,800</strong></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <a href="evatril-addon.php" class="btn btn-outline-primary btn-block"><i class="icon-plus"></i> ADD ON</a>
                </div>
                <div class="col-sm-6 text-right">
                    <button class="btn btn-sm btn-primary px-4" onclick="window.location.href = 'service-confirm-booking.php';">Continue <i class="icon-arrow-right"></i></button>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

</body>
</html>