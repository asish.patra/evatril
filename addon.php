
<?php include 'includes/doctype.php'; ?>

<body>
    
    <?php include 'includes/header.php'; ?>
    <section class="list-inner-content py-3">
    <div class="container">
        <div class="inner-wrapper">
            <div class="inner-content p-sm-4 p-3">
                <div class="row">
                    <div class="col-sm-4 mb-sm-0 mb-3">
                        <h5 class="mb-3 addon-hdng">ADD ON <label class="vegType veg"></label></h5>
                        <div class="package-info add-on-info">
                            <div class="row-ln">
                                <i class="icon-catering"></i>
                                <label for="">Primary Package</label>
                                <strong class="col-red">Dimond Veg</strong>
                            </div>
                            <div class="row-ln">
                                <i class="icon-lunch1"></i>
                                <label>Price <small>(Per Plate)</small></label>
                                <strong><span class="icon-inr"></span> 2,500</strong>
                            </div>
                            <div class="row-ln">
                                <i class="icon-dinning-table"></i>
                                <label>Number of Items</label>
                                <strong>32</strong>
                            </div>
                            <button class="btn btn-sm btn-primary btn-block px-4 my-3" data-toggle="modal" data-target="#advanceModal">Proceed <i class="icon-arrow-right"></i></button>
                        </div>
                    </div>
                    <div class="col-sm-8 pl-sm-0">
                        <table class="table table-borderless add-on-menu-table">
                            <tr>
                                <td><img src="imgs/food-img1.jpg" alt=""></td>
                                <td><img src="imgs/food-img2.jpg" alt=""></td>
                                <td><img src="imgs/food-img3.jpg" alt=""></td>
                            </tr>
                            <tr>
                                <th>Appetizer (Starter)</th>
                                <th>Main Course</th>
                                <th>Dessert</th>
                            </tr>
                            <tr>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Juice" name="Juice">
                                        <label for="Juice">Orange Juice <strong><i class="icon-inr"></i> 30 /-</strong></label>
                                </div>
                                </td>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Pulau" name="Pulau">
                                        <label for="Pulau">Veg Pulau <strong><i class="icon-inr"></i> 40 /-</strong></label>
                                </div>
                                </td>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Vanila" name="Vanila">
                                        <label for="Vanila">Vanila Cream <strong><i class="icon-inr"></i> 40 /-</strong></label>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Juice1" name="Juice">
                                        <label for="Juice1">Orange Juice <strong><i class="icon-inr"></i> 30 /-</strong></label>
                                </div>
                                </td>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Pulau1" name="Pulau">
                                        <label for="Pulau1">Veg Pulau <strong><i class="icon-inr"></i> 40 /-</strong></label>
                                </div>
                                </td>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Vanila1" name="Vanila">
                                        <label for="Vanila1">Vanila Cream <strong><i class="icon-inr"></i> 40 /-</strong></label>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Juice2" name="Juice">
                                        <label for="Juice2">Orange Juice <strong><i class="icon-inr"></i> 30 /-</strong></label>
                                </div>
                                </td>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Pulau2" name="Pulau">
                                        <label for="Pulau2">Veg Pulau <strong><i class="icon-inr"></i> 40 /-</strong></label>
                                </div>
                                </td>
                                <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="Vanila2" name="Vanila">
                                        <label for="Vanila2">Vanila Cream <strong><i class="icon-inr"></i> 40 /-</strong></label>
                                </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-sm btn-primary px-4" data-toggle="modal" data-target="#advanceModal">Proceed <i class="icon-arrow-right"></i></button>
                </div>                  
            </div>
        </div>
    </div>
    </section>
    
    <?php include 'includes/footer.php'; ?>

<!-- Advance Modal -->
<div class="modal fade" id="advanceModal" tabindex="-1" role="dialog" aria-labelledby="advanceModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="advanceModalLabel">Payment Informations</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <?php include 'includes/payment-confirmation.php'; ?>
        </div>
        </div>
    </div>
</div>

</body>
</html>