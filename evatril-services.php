<?php include 'includes/doctype.php'; ?>

<body>

    <?php include 'includes/header.php'; ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 inner-wrapper py-3">
                <div class="container">
                    <div class="inner-content p-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>FAQ (User Guide)</h4>
                                <div class="card faq-card">
                                    <h5>1. How to book a hall?</h5>
                                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nostrum quod placeat nisi voluptatibus consequuntur, culpa saepe molestiae beatae voluptas neque dignissimos tempore rem mollitia explicabo pariatur fuga nam illum iste?</p>

                                    <h5>Is their any Home delivery System ?</h5>
                                    <p>Yes, after selecting any menu, user can select home delivery option or preparation at venue</p>
                                </div>
                                <div class="card ads-card mt-3 mb-sm-0 mb-3">
                                    <img src="imgs/ads.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-sm-8 mt-4 mt-lg-0">
                                <h4>Select Menu</h4>
                                <div class="row service-menu-list">
                                    <div class="col-sm-12 mb-3">
                                        <div class="card">
                                            <a href="services-detail.php">
                                                <img src="imgs/venue-decor.jpg" alt="">
                                            </a>
                                            <div class="text-content">
                                                <p>"Social | Events | Corporate Events"</p>
                                                <a href="evatril-services-detail.php" class="btn btn-outline-primary px-4 btn-sm">Venue Decoration <i class="icon-keyboard_arrow_right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <div class="card">
                                            <a href="services-detail.php">
                                                <img src="imgs/vehicle.jpg" alt="">
                                            </a>
                                            <div class="text-content">
                                                <p>"Sedam | SUV | Mini | SUV"</p>
                                                <a href="evatril-services-detail.php" class="btn btn-outline-primary px-4 btn-sm">Vehicle Decoration <i class="icon-keyboard_arrow_right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <a href="services-detail.php">
                                                <img src="imgs/out-door.jpg" alt="">
                                            </a>
                                            <div class="text-content">
                                                <p>"Tent House | Theme Decoration"</p>
                                                <a href="evatril-services-detail.php" class="btn btn-outline-primary px-4 btn-sm">Outdoor Decoration <i class="icon-keyboard_arrow_right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <h4 class="mt-5">Testimonials</h4>
                                <ul id="testimonials-list">
                                    <li>
                                        <div class="card">
                                            <img src="imgs/man.png" alt="">
                                            <p class="mb-0"><strong>Rahul Sharma</strong></p>
                                            <p><span>Designation</span></p>
                                            <p class="desc">"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore eligendi tempora possimus totam beatae nam mollitia dignissimos."</p>
                                            <label><i class="icon-commenting-o"></i></label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="card">
                                            <img src="imgs/profile.png" alt="">
                                            <p class="mb-0"><strong>Rahul Sharma</strong></p>
                                            <p><span>Designation</span></p>
                                            <p class="desc">illum earum ab numquam soluta fugiat esse commodi at magni modi molestiae cupiditate!</p>
                                            <label><i class="icon-commenting-o"></i></label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="card">
                                            <img src="imgs/user.jpg" alt="">
                                            <p class="mb-0"><strong>Rahul Sharma</strong></p>
                                            <p><span>Designation</span></p>
                                            <p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore eligendi tempora possimus totam beatae nam mollitia dignissimos.</p>
                                            <label><i class="icon-commenting-o"></i></label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="card">
                                            <img src="imgs/man.png" alt="">
                                            <p class="mb-0"><strong>Rahul Sharma</strong></p>
                                            <p><span>Designation</span></p>
                                            <p class="desc">Inventore eligendi tempora possimus totam beatae nam mollitia dignissimos, illum earum ab numquam soluta fugiat esse commodi at magni modi molestiae cupiditate.</p>
                                            <label><i class="icon-commenting-o"></i></label>
                                        </div>
                                    </li>
                                </ul>

                                <div class="card p-3 mt-4 gray-bg">
                                    <h4 class="mb-0">10 Reviews</h4>
                                    <div class="footer-rattings">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star_half"></i>
                                        <i class="icon-star_border"></i>
                                        <i class="icon-star_border"></i>
                                        <label>2.5 / 5</label>
                                        <a href="#">4 Reviews</a>
                                    </div>
                                    <hr>
                                    <div class="review-content-list">
                                        <div class="person py-2">
                                            <label>R</label>
                                            <div>
                                                <p class="mb-0"><strong>Rahul Ku. Sharma</strong>, <small>22 Mar 2020</small></p>
                                                <p class="mb-0">"Quality of food was good and service is also nice."</p>
                                            </div>
                                        </div>
                                        <div class="person py-2">
                                            <label>R</label>
                                            <div>
                                                <p class="mb-0"><strong>Rahul Ku. Sharma</strong>, <small>22 Mar 2020</small></p>
                                                <p class="mb-0">"Quality of food was good and service is also nice."</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'includes/footer.php'; ?>
    <script src="js/lightslider.js"></script>
    <script type="text/javascript">    
        $(document).ready(function() {
            $("#testimonials-list").lightSlider({
                item:2,
                loop:false,
                speed:600,
                responsive : [
                    {
                        breakpoint:800,
                        settings: {
                            item:3,
                            slideMove:1,
                            slideMargin:6,
                        }
                    },
                    {
                        breakpoint:480,
                        settings: {
                            item:2,
                            slideMove:1
                        }
                    }
                ]
            });
        });
    </script>

</body>

</html>